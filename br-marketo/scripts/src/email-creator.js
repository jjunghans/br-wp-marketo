import React from 'react';
import '@wordpress/data';
import '@wordpress/element';
import '@wordpress/edit-post';
import '@wordpress/plugins';
import '@wordpress/components';
import '@wordpress/compose';
import '@wordpress/data';
import '@wordpress/hooks';

jQuery(function() {

    const { data } = wp;
    const { Fragment } = wp.element;
    const { PluginSidebar, PluginSidebarMoreMenuItem } = wp.editPost;
    const { registerPlugin } = wp.plugins;
    const { Notice, Modal, Button } = wp.components;

    class EmailEditorPanel extends React.Component {

        constructor(props) {
            super(props);

            this.state = {
                isPublished: false,
                saveError: false,
                saveSuccess: false,
                // approveSuccess: false,
                // approveError: false,
                // unapproveSuccess: false,
                // unapproveError: false,
                sendTestSuccess: false,
                sendTestError: false,
                marketo_id: 0,
                fromName: '',
                fromEmail: '',
                replyEmail: '',
                subject: '',
                description: '',
                status: '',
                emailHTML: '',
                openSendTestModal: false,
                test_recipients: ''
            };

            this.handleStateChange = this.handleStateChange.bind(this);
            this.mapBlocksToEmailMarkup = this.mapBlocksToEmailMarkup.bind(this);
            this.saveEmail = this.saveEmail.bind(this);
            this.approveEmail = this.approveEmail.bind(this);
            this.unapproveEmail = this.unapproveEmail.bind(this);
            this.openSendTestModal = this.openSendTestModal.bind(this);
            this.closeSendTestModal = this.closeSendTestModal.bind(this);
            this.sendTestEmail = this.sendTestEmail.bind(this);
            this.saveSuccess = this.saveSuccess.bind(this);
            this.saveError = this.saveError.bind(this);
            // this.approveSuccess = this.approveSuccess.bind(this);
            // this.approveError = this.approveError.bind(this);
            // this.unapproveSuccess = this.unapproveSuccess.bind(this);
            // this.unapproveError = this.unapproveError.bind(this);
            this.sendTestSuccess = this.sendTestSuccess.bind(this);
            this.sendTestError = this.sendTestError.bind(this);
        }

        componentDidMount() {
            this.getPostMarketoEmail();
            this.mapBlocksToEmailMarkup();

            data.subscribe(() => {

                this.setState({
                    isPublished: wp.data.select('core/editor' ).isCurrentPostPublished()
                });

                if( wp.data.select('core/editor').hasChangedContent() ) {
                    this.mapBlocksToEmailMarkup();
                }
            });
        }

        getPostMarketoEmail() {
            let form_values = {
                action: 'get_marketo_email',
                postID: wp.data.select('core/editor').getCurrentPostId()
            };

            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: form_values,
                dataType: 'json',
                success: (data) => {
                    if( data.status === 'SUCCESS' ) {
                        this.setState(data.marketo_email);
                    }
                }
            });
        }

        mapBlocksToEmailMarkup() {

            let markup = [];

            $.each( wp.data.select('core/editor').getBlocks(), ( idx,block ) => {
                let markupToAdd = null;

                switch( block.name ) {
                    case 'core/separator':
                        markupToAdd = `
                            <tr> 
                              <td valign="top" align="left" bgcolor="#FFFFFF" style="-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:22px; line-height:24px; color:#1c355d;">
                                <table width="100%" style="width: 100%; min-width: 100%;" align="left" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" role="presentation"> 
                                  <tbody> 
                                    <tr> 
                                      <td valign="top" align="left" width="10%">&nbsp;</td> 
                                      <td width="80%" align="left" bgcolor="#ffffff" style="border-bottom:2px solid #ff4b61; width: 80%; min-width: 80%;">&nbsp;</td> 
                                      <td valign="top" align="left" width="10%">&nbsp;</td> 
                                    </tr> 
                                  </tbody> 
                                </table>
                              </td>
                            </tr>`;
                        break;
                    case 'core/pullquote':
                        markupToAdd = `
                            <tr>
                                <td>
                                    <table width="100%" style="width: 100%; min-width: 100%;" align="left" bgcolor="#FFFFFF" border="0" cellSpacing="0" cellPadding="0" role="presentation">
                                        <tbody>
                                        <tr>
                                            <td bgcolor="#FFFFFF" align="left" valign="top">
                                                <table align="left" border="0" cellPadding="0" cellSpacing="0" width="100%" bgcolor="#FFFFFF" role="presentation">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" valign="top" width="10%">&nbsp;</td>
                                                        <td align="left" style="text-align:left;-webkit-text-size-adjust:none;  padding-top:20px; padding-bottom:5px;" valign="top">
                                                            <table align="left" border="0" cellPadding="0" cellSpacing="0" style="margin:0 auto !important;" width="100%" role="presentation">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" bgcolor="#FFFFFF" style="text-align:left; font-family:Arial, Helvetica, sans-serif; border-collapse: collapse; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; mso-table-lspace:0pt; mso-table-rspace:0pt; font-size:9px; line-height:12px;">
                                                                        <table align="left" border="0" cellPadding="0" cellSpacing="0" style="margin:0 auto !important;" width="100%" role="presentation">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td width="95" align="left" valign="top">
                                                                                    <img src="/index.php/lpTemplate/proxyAsset?idx=eyJpIjoiK3ZyOEgybUs4RlBkNm9vTCIsInQiOiJvbWlkeGRiSHZReDN3M01QbndSU3FuT2NCbGwrQmNNYk1WTU54REYvOWQxa2FmemhBbzBaVmgrYllibHVOZVdhRFZIelN6STBaMzNrcVBWUTNvd3BRdz09In0%3D" alt="quotes" width="88" height="48" style="display:block; height:auto; outline:0; border:none;" border="0" />
                                                                                </td>
                                                                                <td align="left" bgcolor="#FFFFFF" style="text-align:left; font-family:Arial, Helvetica, sans-serif; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; border-spacing: 0; mso-line-height-rule: exactly; mso-margin-bottom-alt: 0; mso-margin-top-alt: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-collapse: collapse; padding:0; margin:0; border:none; color:#1c355d; font-weight:bold; font-size:16px; line-height: 19px;">
                                                                                    <div style="padding: 0; margin: 0;">
                                                                                        &ldquo;${EmailEditorPanel.stripHTML(block.attributes.value)}.&rdquo;
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td align="left" valign="top" width="10%">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" align="left" valign="top">
                                                <table align="left" border="0" cellPadding="0" cellSpacing="0" width="100%" bgcolor="#FFFFFF" role="presentation">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" valign="top" width="10%">&nbsp;</td>
                                                        <td width="80%" align="right">
                                                            <table align="right" role="presentation">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="38%"></td>
                                                                    <td align="right" style="text-align:left; font-family:Arial, Helvetica, sans-serif; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; border-spacing: 0; mso-line-height-rule: exactly; mso-margin-bottom-alt: 0; mso-margin-top-alt: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-collapse: collapse;padding:0;margin:0;border:none; color:#1c355d; font-size:16px; line-height: 19px; ">
                                                                        <div style="padding: 0; margin: 0;">
                                                                            <em>&mdash; ${block.attributes.citation}</em>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td align="left" valign="top" width="10%">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>`;
                        break;
                    case 'core/paragraph':
                        markupToAdd = `
                            <tr> 
                              <td valign="top" align="left" bgcolor="#FFFFFF" style="-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:22px; line-height:24px; color:#1c355d;"> 
                                <table width="100%" style="width: 100%; min-width: 100%;" align="left" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" role="presentation"> 
                                  <tbody> 
                                    <tr> 
                                      <td valign="top" bgcolor="#FFFFFF" align="left"> 
                                        <table width="100%" bgcolor="#FFFFFF" align="left" border="0" cellspacing="0" cellpadding="0" role="presentation"> 
                                          <tbody> 
                                            <tr> 
                                              <td align="left" width="10%">&nbsp;</td> 
                                              <td align="left" valign="top" style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; color:#1c355d; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; border-spacing: 0; mso-line-height-rule: exactly; mso-margin-bottom-alt: 0; mso-margin-top-alt: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-collapse: collapse;padding:0;margin:0;border:none;"> 
                                                <div class="mktoText" id="bottomarticletext">
                                                    ${block.attributes.content}
                                                </div> </td> 
                                              <td align="left" width="10%">&nbsp;</td> 
                                            </tr> 
                                          </tbody> 
                                        </table> </td> 
                                    </tr> 
                                  </tbody> 
                                </table> </td> 
                            </tr>`;
                        break;
                    case 'core/media-text':
                        markupToAdd = `
                            <tr> 
                              <td valign="top" align="left" bgcolor="#FFFFFF" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:22px; font-weight: bold; line-height:24px; color:#1c355d;"> 
                                <table width="100%" style="width: 100%; min-width: 100%;" align="left" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" role="presentation"> 
                                  <tbody> 
                                    <tr> 
                                      <td valign="top" bgcolor="#FFFFFF" align="left"> 
                                        <table width="100%" bgcolor="#FFFFFF" align="left" border="0" cellspacing="0" cellpadding="0" role="presentation"> 
                                          <tbody> 
                                            <tr> 
                                              <td align="left" width="10%">&nbsp;</td> 
                                              <td align="left" valign="top" style="text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight: normal; line-height:19px; color:#1c355d; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; border-spacing: 0; mso-line-height-rule: exactly; mso-margin-bottom-alt: 0; mso-margin-top-alt: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-collapse: collapse;padding:0;margin:0;border:none; padding-bottom:5px; padding-top:0px;"> 
                                                <div> 
                                                  <img src="${block.attributes.mediaURL}" align="left" alt="${block.attributes.mediaAlt}" width="88" height="88" style="display: inline; outline:0; border:none; padding-right:15px; padding-bottom:5px;" border="0" />{block.innerBlocks[0].attributes.content}
                                                </div> </td> 
                                              <td align="left" width="10%">&nbsp;</td> 
                                            </tr> 
                                          </tbody> 
                                        </table> </td> 
                                    </tr> 
                                  </tbody> 
                                </table> </td> 
                            </tr>`;
                        break;
                }

                if( markupToAdd !== null ) {
                    let collapsed = markupToAdd.replace(/(<(pre|script|style|textarea)[^]+?<\/\2)|(^|>)\s+|\s+(?=<|$)/g, "$1$3");
                    markup.push(collapsed);
                }
            });

            this.setState({ emailHTML: markup.join('') });
        }

        handleStateChange(e) {
            this.setState({
                [e.target.name]: e.target.value
            });
        }

        saveEmail() {

            this.setState({ errors: [] });

            const allowed_form_fields = ['fromName','fromEmail','replyEmail','subject','description'];

            let emailHTML = `<table width="100%" style="width: 100%; min-width: 100%;" align="left" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" role="presentation"><tbody>${this.state.emailHTML}</tbody></table>`;

            let form_values = {
                action: 'save_email',
                postID: wp.data.select('core/editor').getCurrentPostId(),
                emailHTML
            };

            for( let x in allowed_form_fields ) {
                const key = allowed_form_fields[x];
                form_values[key] = this.state[key];
            }

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: form_values,
                dataType: 'json',
                success: (data) => {
                    if( data.status === 'SUCCESS' ) {
                        this.setState({
                            marketo_id: data.marketo_id
                        });

                        this.saveSuccess();
                    } else {
                        this.saveError(data.message);
                    }
                }
            });

        }

        approveEmail() {
            let form_values = {
                action: 'approve_email',
                postID: wp.data.select('core/editor').getCurrentPostId()
            };

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: form_values,
                dataType: 'json',
                success: (data) => {
                    if( data.status === 'SUCCESS' ) {
                        this.setState({
                            status: 'approved'
                        });

                        this.approveSuccess();
                    } else {
                        this.approveError(data.message);
                    }
                }
            });
        }

        unapproveEmail() {
            let form_values = {
                action: 'unapprove_email',
                postID: wp.data.select('core/editor').getCurrentPostId()
            };

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: form_values,
                dataType: 'json',
                success: (data) => {
                    if( data.status === 'SUCCESS' ) {
                        this.setState({
                            status: 'draft'
                        });

                        this.unapproveSuccess();
                    } else {
                        this.unapproveError(data.message);
                    }
                }
            });
        }

        openSendTestModal() {
            this.setState({
                openSendTestModal: true
            });
        }

        closeSendTestModal() {
            this.setState({
                openSendTestModal: false,
                test_recipients: ''
            });
        }

        sendTestEmail() {
            let form_values = {
                action: 'send_test_email',
                postID: wp.data.select('core/editor').getCurrentPostId(),
                recipients: this.state.test_recipients
            };

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: form_values,
                dataType: 'json',
                success: (data) => {
                    if( data.status === 'SUCCESS' ) {
                        this.sendTestSuccess();
                    } else {
                        this.sendTestError(data.message);
                    }
                }
            });
        }

        saveSuccess() {
            this.setState({
                saveSuccess: true
            });

            setTimeout(() => {
                this.setState({
                    saveSuccess: false
                });
            }, 2000);
        }

        saveError(err) {
            this.setState({
                saveError: err
            });

            setTimeout(() => {
                this.setState({
                    saveError: false
                });
            }, 2000);
        }

        // approveSuccess() {
        //     this.setState({
        //         approveSuccess: true
        //     });
        //
        //     setTimeout(() => {
        //         this.setState({
        //             approveSuccess: false
        //         });
        //     }, 2000);
        // }
        //
        // approveError(err) {
        //     this.setState({
        //         approveError: err
        //     });
        //
        //     setTimeout(() => {
        //         this.setState({
        //             approveError: false
        //         });
        //     }, 2000);
        // }
        //
        // unapproveSuccess() {
        //     this.setState({
        //         unapproveSuccess: true
        //     });
        //
        //     setTimeout(() => {
        //         this.setState({
        //             unapproveSuccess: false
        //         });
        //     }, 2000);
        // }
        //
        // unapproveError(err) {
        //     this.setState({
        //         unapproveError: err
        //     });
        //
        //     setTimeout(() => {
        //         this.setState({
        //             unapproveError: false
        //         });
        //     }, 2000);
        // }

        sendTestSuccess() {
            this.setState({
                sendTestSuccess: true
            });

            setTimeout(() => {
                this.setState({
                    sendTestSuccess: false
                });
            }, 2000);
        }

        sendTestError(err) {
            this.setState({
                sendTestError: err
            });

            setTimeout(() => {
                this.setState({
                    sendTestError: false
                });
            }, 2000);
        }

        render() {
            return (
                <Fragment>
                    <PluginSidebarMoreMenuItem target="email-editor-sidebar">Email Editor</PluginSidebarMoreMenuItem>
                    <PluginSidebar name="email-editor-sidebar" title="Email Editor">
                        {this.state.saveSuccess && (
                            <Notice status="success">
                                <p>Email Saved.</p>
                            </Notice>
                        )}
                        {this.state.saveError && (
                            <Notice status="error">
                                <p>{this.state.saveError}</p>
                            </Notice>
                        )}
                        {/*{this.state.approveSuccess && (*/}
                        {/*    <Notice status="success">*/}
                        {/*        <p>Email Approved.</p>*/}
                        {/*    </Notice>*/}
                        {/*)}*/}
                        {/*{this.state.approveError && (*/}
                        {/*    <Notice status="error">*/}
                        {/*        <p>{this.state.approveError}</p>*/}
                        {/*    </Notice>*/}
                        {/*)}*/}
                        {/*{this.state.unapproveSuccess && (*/}
                        {/*    <Notice status="success">*/}
                        {/*        <p>Email Unapproved.</p>*/}
                        {/*    </Notice>*/}
                        {/*)}*/}
                        {/*{this.state.unapproveError && (*/}
                        {/*    <Notice status="error">*/}
                        {/*        <p>{this.state.unapproveError}</p>*/}
                        {/*    </Notice>*/}
                        {/*)}*/}
                        {this.state.isPublished ? (
                            <div className='components-panel__body is-opened'>
                                <div className="components-base-control">
                                    <div className="components-base-control__field">
                                        <label className="components-base-control__label">From Name</label>
                                        <input name="fromName" className="components-text-control__input" type="text" value={ this.state.fromName } onChange={ this.handleStateChange } />
                                    </div>
                                </div>
                                <div className="components-base-control">
                                    <div className="components-base-control__field">
                                        <label className="components-base-control__label">From Email</label>
                                        <input name="fromEmail" className="components-text-control__input" type="text" value={ this.state.fromEmail } onChange={ this.handleStateChange } />
                                    </div>
                                </div>
                                <div className="components-base-control">
                                    <div className="components-base-control__field">
                                        <label className="components-base-control__label">Reply Email</label>
                                        <input name="replyEmail" className="components-text-control__input" type="text" value={ this.state.replyEmail } onChange={ this.handleStateChange } />
                                    </div>
                                </div>
                                <div className="components-base-control">
                                    <div className="components-base-control__field">
                                        <label className="components-base-control__label">Subject</label>
                                        <input name="subject" className="components-text-control__input" type="text" value={ this.state.subject } onChange={ this.handleStateChange } />
                                    </div>
                                </div>
                                <div className="components-base-control">
                                    <div className="components-base-control__field">
                                        <label className="components-base-control__label">Description</label>
                                        <textarea name="description" className="components-textarea-control__input" value={ this.state.description } onChange={ this.handleStateChange } />
                                    </div>
                                </div>
                                <Button isLink onClick={ this.openSendTestModal }>Send Test</Button>
                                { this.state.openSendTestModal && (
                                    <Modal title="Send Test Email" onRequestClose={ this.closeSendTestModal }>
                                        {this.state.sendTestSuccess && (
                                            <Notice status="success">
                                                <p>Test email sent.</p>
                                            </Notice>
                                        )}
                                        {this.state.sendTestError && (
                                            <Notice status="error">
                                                <p>{this.state.sendTestError}</p>
                                            </Notice>
                                        )}
                                        <p>Enter an email address to send a test to.</p>
                                        <div className="components-base-control">
                                            <div className="components-base-control__field">
                                                <input name="test_recipients" className="components-text-control__input" value={ this.state.test_recipients } type="text" onChange={ this.handleStateChange } />
                                            </div>
                                        </div>
                                        <Button isPrimary onClick={ this.sendTestEmail }>Send Test</Button>
                                    </Modal>
                                )}

                                {/*{this.state.status !== 'approved' ?*/}
                                {/*    (*/}
                                {/*        <Button isDefault onClick={ this.approveEmail }>Approve</Button>*/}
                                {/*    ) : (*/}
                                {/*        <Button isDefault onClick={ this.unapproveEmail }>Unapprove</Button>*/}
                                {/*    )*/}
                                {/*}*/}
                                &nbsp;<Button isPrimary onClick={ this.saveEmail }>Save</Button>
                            </div>
                        ) : (
                            <Notice status="info" isDismissible={false}>
                                Please publish the post to continue
                            </Notice>
                        )}
                    </PluginSidebar>
                </Fragment>
            )
        }

        static stripHTML(html) {
            let tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }
    }

    registerPlugin( 'email-editor', {
        icon: 'email',
        render: EmailEditorPanel,
    });

});