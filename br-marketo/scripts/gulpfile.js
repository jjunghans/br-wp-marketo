const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

gulp.task( 'email-creator-js', () => {
    return gulp.src('src/email-creator.js')
        .pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest('./dist/js'));

});

gulp.task( 'watch', () => {
    gulp.watch( 'src/email-creator.js', gulp.series('email-creator-js') );
});