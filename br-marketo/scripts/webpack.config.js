module.exports = {
    stats: 'errors-only',
    output: {
        filename: 'email-creator.js',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: [
                        ['latest', { modules: false }],
                        'react'
                    ],
                    plugins: [
                        'transform-object-rest-spread'
                    ]
                },
            },
        ],
    },
};