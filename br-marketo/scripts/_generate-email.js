(function($) {
    $(document).ready(function() {

        var $marketo_email_form = $('#marketo-email-form');

        $('#marketo-email-form').on('submit', function(e) {
            e.preventDefault();

            $marketo_email_form.find('.notice').remove();

            var errors = 0,
                allowed_form_fields = ['id','marketo_id','content_id','name','from_name','from_email','reply_email','subject','description','email_content'];

            var form_values = {
                action: 'save_email'
            };

            $.each( allowed_form_fields, function(idx, el_name) {
                var $input = $marketo_email_form.find('[name="brwpmarketo_email['+el_name+']"]');

                if( $input.length ) {
                    form_values[el_name] = $input.val();
                }
            });

            if( errors === 0 ) {
                $.ajax({
                    url: ajaxurl,
                    data: form_values,
                    dataType: 'json',
                    type: 'POST',
                    success: function(data) {
                        if( data.status === 'FAIL' ) {
                            $marketo_email_form.prepend('<div class="notice notice-error"><p><strong>'+data.message+'</strong></p></div>');
                        } else {
                            $marketo_email_form.prepend('<div class="notice notice-success"><p><strong>Email saved!</strong></p></div>');
                        }
                    }
                });
            }

        });

    });
})(jQuery);