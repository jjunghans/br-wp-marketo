<?php

/*
Plugin Name: BR Marketo
Description: API Integration for Marketo
Version: 1.0.0
Author: Bader-Rutter
License: GPLv2 or later
Text Domain: brwpmarketo
*/

use BRMarketo\Credentials;
use BRMarketo\RestClient;

error_reporting(E_ALL);
ini_set('display_errors',true);

// This should never be called directly
if ( !function_exists( 'add_action' ) ) {
    exit;
}

define( 'BRMARKETO_VERSION', '1.1.1' );
define( 'BRMARKETO__MINIMUM_WP_VERSION', '4.0' );
define( 'BRMARKETO__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'BRMARKETO__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'BRMARKETO__TEXT_DOMAIN', 'brwpmarketo' );

$brWpMarketoRestClient = null;

// load the BR Marketo package
require_once( BRMARKETO__PLUGIN_DIR . '_inc/br-marketo/vendor/autoload.php' );

// load the plugin init class
require_once( BRMARKETO__PLUGIN_DIR . 'lib/BRWPMarketo.php' );

// Run activation processes
register_activation_hook( __FILE__, array( 'BRWPMarketo', 'plugin_activation' ) );

// Run deactivation processes
register_deactivation_hook( __FILE__, array( 'BRWPMarketo', 'plugin_deactivation' ) );

// Run site init processes
add_action( 'init', array( 'BRWPMarketo', 'init' ) );

// Load assets if in admin area
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {

    $options = get_option( 'brwpmarketo_config' );
    $munchkin_id = isset( $options['munchkin_id'] ) ? $options['munchkin_id'] : '';
    $client_id = isset( $options['client_id'] ) ? $options['client_id'] : '';
    $client_secret = isset( $options['client_secret'] ) ? $options['client_secret'] : '';

    try {
        $c = new Credentials($munchkin_id, $client_id, $client_secret);
        $brWpMarketoRestClient = new RestClient($c);

        if( !is_a( $brWpMarketoRestClient, '\BRMarketo\RestClient' ) ) {
            throw new \Exception();
        }
    } catch( \Exception $e ) {
        add_action( 'admin_notices', 'brRestClientAdminNotice' );
    }


    // Load helper classes
    require_once( BRMARKETO__PLUGIN_DIR . 'lib/models/Email.php' );
    require_once( BRMARKETO__PLUGIN_DIR . 'lib/OptionsPage.php' );

    // load option page controllers
    require_once( BRMARKETO__PLUGIN_DIR . 'views/Base.php' );
    require_once( BRMARKETO__PLUGIN_DIR . 'views/Config.php' );
    require_once( BRMARKETO__PLUGIN_DIR . 'views/Email.php' );
    require_once( BRMARKETO__PLUGIN_DIR . 'views/GenerateEmail.php' );

    // load the plugin admin inti class
    require_once( BRMARKETO__PLUGIN_DIR . 'lib/BRWPMarketoAdmin.php' );

    // Run site init processes
    add_action( 'init', array( 'BRWPMarketoAdmin', 'init' ) );
}

function brRestClientAdminNotice() {
    $class = 'notice notice-error';
    $message = __( 'Missing required Marketo API credentials', 'brwpmarketo' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
}