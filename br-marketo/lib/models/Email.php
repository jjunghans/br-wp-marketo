<?php

namespace BRWPMarketo\Model;

class Email {

    protected $id;

    protected $name;

    protected $user_id;

    protected $date_created;

    protected $marketo_id;

    protected $marketo_email;

    protected $marketo_email_content;

    public function __construct( $id = 0 ) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'brwpmarketo_emails';

        $result = $wpdb->get_row($wpdb->prepare("SELECT id,name,user_id,date_created,marketo_id FROM {$table_name} WHERE id = %d LIMIT 1", $id));

        if( $result ) {
            $this->id = $result->id;
            $this->name = $result->name;
            $this->user_id = $result->user_id;
            $this->date_created = $result->date_created;
            $this->marketo_id = $result->marketo_id;
        }
    }

    public function set( $key, $value ) {
        $this->{$key} = $value;
    }

    public function get( $key ) {
        return $this->{$key};
    }

    public function getMarketoEmail() {
        try {
            global $brWpMarketoRestClient;
            return $brWpMarketoRestClient->getEmailByID($this->marketo_id);
        } catch( \Exception $e ) {

        }
    }

    public function getMarketoEmailContent() {
        try {
            global $brWpMarketoRestClient;
            return $brWpMarketoRestClient->getEditableSections($this->marketo_id);
        } catch( \Exception $e ) {

        }
    }

    public function getFullContent() {
        try {
            global $brWpMarketoRestClient;
            return $brWpMarketoRestClient->getFullContent($this->marketo_id);
        } catch( \Exception $e ) {

        }
    }

    public function save( $props = [] ) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'brwpmarketo_emails';

        if( isset( $props['marketo_id'] ) ) {
            $this->marketo_id = $props['marketo_id'];
        }

        if( isset( $props['name'] ) ) {
            $this->name = $props['name'];
        }

        if( $this->id != null ) {
            $result = $wpdb->update( $table_name, [ 'name' => $this->name ], [ 'id' => $this->id ] );
        } else {
            $result = $wpdb->insert( $table_name, [
                'name' => $this->name,
                'user_id' => get_current_user_id(),
                'marketo_id' => $this->marketo_id
            ]);

            $this->id = $this->id = $wpdb->insert_id;
        }

        if( $result === false ) {
            throw new \Exception( __( 'The record could not be created', 'brwpmarketo' ) );
        }
    }

    public static function getAll() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'brwpmarketo_emails';
        $results = $wpdb->get_results("SELECT id FROM {$table_name}" );
        $emails = [];

        if( !is_array( $results ) && !$results ) {
            throw new \Exception( __( "Could not get stored emails: $wpdb->last_error" ) );
        } else {
            foreach( $results as $result ) {
                $emails[] = new Email( $result->id );
            }
        }

        return $emails;
    }

}