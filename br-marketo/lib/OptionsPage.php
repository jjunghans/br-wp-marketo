<?php

namespace BRWPMarketo\OptionsPage;

class OptionsPage {

    protected $errors = [];

    public function __construct() {
        add_action( 'admin_notices', array( $this, 'display_notices' ) );
    }

    public function display_notices() {

        if( count( $this->errors ) > 0 ) {
            foreach( $this->errors as $err ) {
                $class = 'notice notice-error';
                $message = __( $err, 'brwpmarketo' );
                printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
            }
        }
    }

}