<?php

class BRWPMarketo {

    // Has an instance already been created
    private static $initiated = false;

    public static function init() {
        if ( ! self::$initiated ) {
            self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        self::$initiated = true;
    }

    /**
     * Run processes for plugin activation
     */
    public static function plugin_activation() {
        if ( version_compare( $GLOBALS['wp_version'], BRMARKETO__MINIMUM_WP_VERSION, '<' ) ) {
            //@todo: Run version check
        } else {
            $stored_version = get_option( 'brwpmarketo_version', '1.0.0' );

            self::createEmailsTable();

            if( version_compare( $stored_version, BRMARKETO_VERSION, '<' ) ) {
                self::update_plugin();
                update_option( 'brwpmarketo_version', BRMARKETO_VERSION );
            }
        }
    }

    /**
     * Run processes for plugin deactivation
     */
    public static function plugin_deactivation() {}

    /**
     * Creates the emails table
     */
    protected static function createEmailsTable() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'brwpmarketo_emails';

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          name varchar(255) NOT NULL,
          marketo_id varchar(255) NOT NULL,
          user_id int(11) NOT NULL,
          date_created datetime DEFAULT NOW() NOT NULL,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    private static function update_plugin() {

    }
}