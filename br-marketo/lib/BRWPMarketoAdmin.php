<?php

use BRWPMarketo\Model\Email as SavedEmails;
use BRWPMarketo\OptionsPage\Base;
use BRWPMarketo\OptionsPage\Config;
use BRWPMarketo\OptionsPage\Email;
use BRWPMarketo\Metabox\Email as MetaboxEmail;

class BRWPMarketoAdmin {

    private static $initiated = false;

    public static function init() {
        if ( ! self::$initiated ) {
            self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        self::$initiated = true;

        add_action( 'admin_init', [ self::class, 'admin_init' ] );
        add_action( 'enqueue_block_editor_assets', [ self::class, 'enqueue_scripts' ] );
        add_action( 'wp_ajax_save_email', [ self::class, 'save_email' ] );
        add_action( 'wp_ajax_get_marketo_email', [ self::class, 'get_marketo_email' ] );
        add_action( 'wp_ajax_approve_email', [ self::class, 'approve_email' ] );
        add_action( 'wp_ajax_unapprove_email', [ self::class, 'unapprove_email' ] );
        add_action( 'wp_ajax_send_test_email', [ self::class, 'send_test_email' ] );

        $base_page = new Base();
        $config_page = new Config();
        $email_page = new Email();
    }

    public static function admin_init() {
        load_plugin_textdomain( BRMARKETO__TEXT_DOMAIN );
    }

    public static function enqueue_scripts() {
        wp_enqueue_script('brwpmarketo-admin-toolbar-js', BRMARKETO__PLUGIN_URL . 'scripts/dist/js/email-creator.js' );
    }

    public static function get_marketo_email() {
        $data = $_GET;
        $response = [];

        try {
            global $brWpMarketoRestClient;

            // Get the post object
            $post = get_post( $data['postID'] );

            // Get the post's metadata
            $marketoEmailID = get_post_meta( $post->ID, 'marketo_id', true );

            // Get the email's object from Marketo
            $email = $brWpMarketoRestClient->getEmailByID( $marketoEmailID );

            // Build an array of the data needed
            $emailData = [
                'marketo_id' => $marketoEmailID,
                'fromName' => $email->getFromName()['value'],
                'fromEmail' => $email->getFromEmail()['value'],
                'replyEmail' => $email->getReplyEmail()['value'],
                'subject' => $email->getSubject()['value'],
                'description' => $email->getDescription(),
                'status' => $email->getStatus()
            ];

            $response = array( 'status' => 'SUCCESS', 'marketo_email' => $emailData );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }

    public static function save_email() {
        $data = $_POST;
        $response = array();

        try {
            global $brWpMarketoRestClient;
            $options = get_option( 'brwpmarketo_config' );

            // Get the WP_Post object
            $post = get_post( $data['postID'] );

            // Get all the post's meta data
            $postMeta = get_post_meta( $data['postID'] );

            if( !array_key_exists( '_wp_page_template', $postMeta) ) {
                throw new Exception( __( 'Please select a page template and save.', BRMARKETO__TEXT_DOMAIN ) );
            }

            // Get the template for the post
            $postTemplate = $postMeta['_wp_page_template'][0];

            // If no template is chosen, throw an error
            if( is_null( $postTemplate ) || empty( $postTemplate ) ) {
                throw new Exception( __( 'Please choose a template', BRMARKETO__TEXT_DOMAIN ) );
            }

            $template_map = $options['templates'];

            if( !array_key_exists( $postTemplate, $template_map ) ) {
                throw new Exception( __( 'This page template has not been mapped to a Marketo email template.  Click <a href="/wp-admin/admin.php?page=brwpmarketo-congif" target="_blank">here</a> to complete the mapping.', BRMARKETO__TEXT_DOMAIN ) );
            }

            // Get the Marketo ID
            $postMarketoID = array_key_exists( 'marketo_id', $postMeta ) ? (int) $postMeta['marketo_id'][0] : 0;

            //@todo: map post templates to Marketo templates
            $marketoTemplateID = $template_map[$postTemplate];

            //@todo: Get the folder ID from the config
            $marketoFolderID = $options['folder'];

            // Build the name for the email
            $marketoEmailName = $post->post_title . ' - ' . $post->post_date;

            if( !$postMarketoID ) {
                $created = $brWpMarketoRestClient->createEmail(
                    $marketoTemplateID,
                    [ 'id' => $marketoFolderID, 'type' => 'Folder' ],
                    $marketoEmailName,
                    [
                        'fromEmail' => $data['fromEmail'],
                        'fromName' => $data['fromName'],
                        'replyEmail' => $data['replyEmail'],
                        'subject' => $data['subject'],
                        'description' => $data['description']
                    ]
                );

                $postMarketoID = $created->getID();

                update_post_meta( $post->ID, 'marketo_id', $postMarketoID);
            } else {
                // Update the standard data for the email
                $brWpMarketoRestClient->updateEmail( $postMarketoID, [
                    'name' => $marketoEmailName,
                    'description' => $data['description']
                ]);

                // Update the extra data for the email
                $brWpMarketoRestClient->updateEmailContent( $postMarketoID, [
                    'fromName' => json_encode( [ 'type' => 'Text', 'value' => $data['fromName'] ] ),
                    'fromEmail' => json_encode( [ 'type' => 'Text', 'value' => $data['fromEmail'] ] ),
                    'replyTo' => json_encode( [ 'type' => 'Text', 'value' => $data['replyEmail'] ] ),
                    'subject' => json_encode( [ 'type' => 'Text', 'value' => $data['subject'] ])
                ]);
            }

            // Update the editable section
            $brWpMarketoRestClient->setEditableSection( $postMarketoID, 'content', 'Text', stripslashes($data['emailHTML']) );

//
//            $new_email = new SavedEmails();
//            $new_email->save(array(
//                'marketo_id' => $created->getID(),
//                'name' => $created->getName()
//            ));
//
            $response = array( 'status' => 'SUCCESS', 'marketo_id' => $postMarketoID );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }

    public static function approve_email() {
        $data = $_POST;
        $response = [];

        try {
            global $brWpMarketoRestClient;

            // Get the post object
            $post = get_post( $data['postID'] );

            // Get the post's metadata
            $marketoEmailID = get_post_meta( $post->ID, 'marketo_id', true );

            // Send approval
            $brWpMarketoRestClient->approveEmail( $marketoEmailID );

            $response = array( 'status' => 'SUCCESS' );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }

    public static function unapprove_email() {
        $data = $_POST;
        $response = [];

        try {
            global $brWpMarketoRestClient;

            // Get the post object
            $post = get_post( $data['postID'] );

            // Get the post's metadata
            $marketoEmailID = get_post_meta( $post->ID, 'marketo_id', true );

            // Send approval
            $brWpMarketoRestClient->unapproveEmail( $marketoEmailID );

            $response = array( 'status' => 'SUCCESS' );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }

    public static function send_test_email() {
        $data = $_POST;
        $response = [];

        try {
            global $brWpMarketoRestClient;

            // Get the post object
            $post = get_post( $data['postID'] );

            // Get the post's metadata
            $marketoEmailID = get_post_meta( $post->ID, 'marketo_id', true );

            // Send approval
            $brWpMarketoRestClient->sendSampleEmail( $marketoEmailID, $data['recipients'] );

            $response = array( 'status' => 'SUCCESS' );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }
}