<?php

namespace BRWPMarketo\OptionsPage;

use BRWPMarketo\Model\Email as SavedEmails;

class Email extends OptionsPage {

    public function __construct() {
        parent::__construct();
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
//        add_action( 'wp_ajax_create_email', array( $this, 'create_email' ) );
//        add_action( 'wp_ajax_nopriv_save_email', array( $this, 'save_email' ) );
    }

    public function add_plugin_page() {
        add_submenu_page(
            'brwpmarketo',
            __( 'Email', 'brwpmarketo' ),
            __( 'Email', 'brwpmarketo' ),
            'manage_options',
            'brwpmarketo-email',
            array( $this, 'display_page' )
        );
    }

    public function enqueue_scripts() {
        if (isset( $_GET['page'] ) && $_GET['page'] === 'brwpmarketo-email' && isset( $_GET['id'] ) ) {
            wp_enqueue_script(
                'brwpmarketo-generate-email',
                BRMARKETO__PLUGIN_URL . 'scripts/generate-email.js',
                array('jquery'),
                '1.0.0',
                true
            );
        }
    }

    public function display_page() {
        if( isset( $_GET['id'] ) ) {
            $this->display_single_page();
        } else {
            $this->display_list_page();
        }
    }

    public function display_list_page() {
        add_thickbox();
        $config_options = get_option('brwpmarketo_config');
        $emails = SavedEmails::getAll();
        $templates = $config_options['templates'];
        $folders = $config_options['folders'];
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?php echo __( 'BR WP Marketo - Emails', 'brwpmarketo' ); ?></h1>
            <a name="Create Email" href="#TB_inline?&width=600&height=550&inlineId=new_email_form_wrapper" class="page-title-action thickbox">Add New</a>
            <hr class="wp-header-end">
            <table class="wp-list-table widefat fixed striped" style="margin-top:20px;">
                <thead></thead>
                <tbody>
                <?php if( count( $emails ) < 1 ) { ?>
                    <tr>
                        <th scope="row" style="text-align:center;"><?php echo __( 'No emails to display', 'brwpmarketo' ); ?></th>
                    </tr>
                <?php } else { ?>
                    <?php foreach( $emails as $email ) { ?>
                    <tr>
                        <th scope="row">
                            <a href="/wp-admin/admin.php?page=brwpmarketo-email&id=<?php echo $email->get('id'); ?>"><?php echo $email->get('name'); ?></a>
                        </th>
                    </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>
        <div id="new_email_form_wrapper" style="display:none;">
            <form id="new_email_form" method="post" action="options.php">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_template_id"><?php echo __( 'Template', 'brwpmarketo' ); ?></label>
                        </th>
                        <td>
                            <select id="new_email_form_template_id" name="new_email_form[template_id]">
                                <option value=""><?php echo __( 'Select a template...', 'brwpmarketo' ); ?>*</option>
                                <?php foreach( $templates as $template ) { ?>
                                <option value="<?php echo $template['id']; ?>"><?php echo $template['name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_folder_id"><?php echo __( 'Folder', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <select id="new_email_form_folder_id" name="new_email_form[folder_id]">
                                <option value=""><?php echo __( 'Select a folder...', 'brwpmarketo' ); ?></option>
                                <?php foreach( $folders as $folder ) { ?>
                                <option value="<?php echo $folder['id']; ?>"><?php echo $folder['name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_name"><?php echo __( 'Name', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <input type="text" id="new_email_form_name" class="regular-text" name="new_email_form[name]">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_from_name"><?php echo __( 'From Name', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <input type="text" id="new_email_form_from_name" class="regular-text" name="new_email_form[from_name]">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_from_email"><?php echo __( 'From Email', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <input type="text" id="new_email_form_from_email" class="regular-text" name="new_email_form[from_email]">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_reply_email"><?php echo __( 'Reply From', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <input type="text" id="new_email_form_reply_email" class="regular-text" name="new_email_form[reply_email]">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_subject"><?php echo __( 'Subject', 'brwpmarketo' ); ?>*</label>
                        </th>
                        <td>
                            <input type="text" id="new_email_form_subject" class="regular-text" name="new_email_form[subject]">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="new_email_form_description"><?php echo __( 'Description', 'brwpmarketo' ); ?> <small><em>(<?php echo __( 'Optional', 'brwpmarketo' ); ?>)</em></small></label>
                        </th>
                        <td>
                            <textarea id="new_email_form_description" class="regular-text" name="new_email_form[description]"></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php submit_button( __( 'Create Email', 'brwpmarketo' ) ); ?>
            </form>
        </div>
        <script type="text/javascript">
            (function($) {
                $(document).ready(function() {

                    var $new_email_form = $('#new_email_form');

                    $new_email_form.on('submit', function(e) {
                        e.preventDefault();

                        $new_email_form.find('.notice').remove();
                        $new_email_form.find('.error').removeClass('error');

                        var errors = 0;

                        var allowed_form_fields = ['template_id','folder_id','name','from_name','from_email','reply_email','subject','description'],
                            required_fields = ['template_id','folder_id','name','from_name','from_email','reply_email','subject'];

                        var form_values = {
                            action: 'create_email'
                        };

                        $.each( allowed_form_fields, function(idx, el_name) {
                            var has_err = false,
                                $input = $new_email_form.find('[name="new_email_form['+el_name+']"]');

                            if( $input.length ) {
                                var is_required_field = required_fields.filter(function(name) {
                                    return name === el_name;
                                }).length > 0;

                                if( is_required_field ) {
                                    if( $input.val() === null || $input.val() === '' ) {
                                        $input.addClass('error');
                                        errors++;
                                        has_err = true;
                                    }
                                }

                                if( !has_err ) {
                                    form_values[el_name] = $input.val();
                                }
                            }
                        });

                        if( errors === 0 ) {
                            $.ajax({
                                url: ajaxurl,
                                type: 'POST',
                                data: form_values,
                                dataType: 'json',
                                success: function(data) {
                                    if( data.status === 'FAIL' ) {
                                        $new_email_form.prepend('<div class="notice notice-error"><p><strong>'+data.message+'</strong></p></div>');
                                    } else {
                                        window.location.replace(window.location.href + '&id=' + data.id);
                                    }
                                }
                            });
                        }
                    });

                });
            })(jQuery);
        </script>
        <?php
    }

    public function display_single_page() {
        $email = new SavedEmails( $_GET['id'] );
        $marketoEmail = $email->getMarketoEmail();
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?php echo __( 'BR WP Marketo - Emails', 'brwpmarketo' ); ?></h1>
            <hr class="wp-header-end">
            <form id="marketo-email-form" method="post">
                <input type="hidden" name="brwpmarketo_email[id]" value="<?php echo $email->get('id'); ?>">
                <input type="hidden" name="brwpmarketo_email[marketo_id]" value="<?php echo $marketoEmail->getID(); ?>">
                <?php settings_fields( 'brwpmarketo' ); ?>
                <div class="brwpmarketo-columns" style="padding-top:20px;">
                    <div class="brwpmarketo-aside">

                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_name"><?php echo __( 'Name', 'brwpmarketo' ); ?></label>
                            <input type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_name" name="brwpmarketo_email[name]" value="<?php echo $marketoEmail->getName(); ?>" />
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_from_name"><?php echo __( 'From Name', 'brwpmarketo' ); ?></label>
                            <input type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_from_name" name="brwpmarketo_email[from_name]" value="<?php echo $marketoEmail->getFromName()['value']; ?>" />
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_from_email"><?php echo __( 'From Email', 'brwpmarketo' ); ?></label>
                            <input type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_from_email" name="brwpmarketo_email[from_email]" value="<?php echo $marketoEmail->getFromEmail()['value']; ?>" />
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_reply_email"><?php echo __( 'Reply To Email', 'brwpmarketo' ); ?></label>
                            <input type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_reply_email" name="brwpmarketo_email[reply_email]" value="<?php echo $marketoEmail->getReplyEmail()['value']; ?>" />
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_subject"><?php echo __( 'Subject', 'brwpmarketo' ); ?></label>
                            <input type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_subject" name="brwpmarketo_email[subject]" value="<?php echo $marketoEmail->getSubject()['value']; ?>" />
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_description"><?php echo __( 'Description', 'brwpmarketo' ); ?></label>
                            <textarea type="text" class="brwpmarketo-aside-field-text__input" id="brwpmarketo_email_description" name="brwpmarketo_email[description]"><?php echo $marketoEmail->getDescription(); ?></textarea>
                        </div>
                        <div class="brwpmarketo-aside-field">
                            <label class="brwpmarketo-aside-field__label" for="brwpmarketo_email_status"><?php echo __( 'Status', 'brwpmarketo' ); ?></label>
                            <select class="brwpmarketo-aside-field-select" id="brwpmarketo_email_status" name="brwpmarketo_email[status]">
                                <option value="draft"<?php selected( $marketoEmail->getStatus(), 'draft'); ?>><?php echo __( 'Draft', 'brwpmarketo' ); ?></option>
                                <option value="approved"<?php selected( $marketoEmail->getStatus(), 'approved'); ?>><?php echo __( 'Approved', 'brwpmarketo' ); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="brwpmarketo-content" id="brwpmarketo-email-generator" style="max-width:700px;overflow:auto;">
                        <?php echo '<pre>'.print_r( '', true ) . '</pre>'; ?>
                    </div>
                </div>
                <?php submit_button( __( 'Save Email', 'brwpmarketo' ) ); ?>
            </form>
        </div>
        <?php
    }

    public function create_email() {
        $data = $_POST;
        $response = array();

        try {
            global $brWpMarketoRestClient;

            $created = $brWpMarketoRestClient->createEmail(
                $data['template_id'],
                [ 'id' => $data['folder_id'], 'type' => 'Folder' ],
                $data['name'],
                [
                    $data['from_email'],
                    $data['from_name'],
                    $data['reply_email'],
                    $data['subject'],
                    $data['description']
                ]
            );

            $new_email = new SavedEmails();
            $new_email->save(array(
                'marketo_id' => $created->getID(),
                'name' => $created->getName()
            ));

            $response = array( 'status' => 'SUCCESS', 'id' => $new_email->get('id') );
        } catch( \Exception $e ) {
            $response = array( 'status' => 'FAIL', 'message' => $e->getMessage() );
        }

        header('Content-Type: application/json' );
        echo json_encode($response);
        wp_die();
    }

    public function save_email() {
        $data = $_POST;
        $response = [];

        $email = new SavedEmails( $data['id'] );
        $email->set( 'name', $data['name'] );

        try {
            global $brWpMarketoRestClient;
            // update the email's metadata in Marketo

            $brWpMarketoRestClient->updateEmail( $email->get('marketo_id'), [
                'name' => $data['name'],
                'description' => $data['description']
            ]);

            // update the content of the email
            $brWpMarketoRestClient->updateEmailContent($email->get('marketo_id'), [
                'fromName' => json_encode( [ 'type' => 'Text', 'value' => $data['from_name'] ] ),
                'fromEmail' => json_encode( [ 'type' => 'Text', 'value' => $data['from_email'] ] ),
                'replyTo' => json_encode( [ 'type' => 'Text', 'value' => $data['reply_email'] ] ),
                'subject' => json_encode( [ 'type' => 'Text', 'value' => $data['subject'] ])
            ]);

            // update the editable section of the email
            $brWpMarketoRestClient->setEditableSection( $email->get('marketo_id'), $data['content_id'], 'Text', $data['email_content'] );

            $email->save();

            $response = [ 'status' => 'SUCCESS', 'id' => $email->get('id') ];
        } catch( \Exception $e ) {
            $response = [ 'status' => 'FAIL', 'message' => $e->getMessage() ];
        }

        header('Content-Type: application/json' );
        echo json_encode( $response );
        wp_die();
    }

}