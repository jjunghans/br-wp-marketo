<?php

namespace BRWPMarketo\OptionsPage;

class Base extends OptionsPage {

    public function __construct() {
        parent::__construct();

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    }

    public function add_plugin_page() {
        add_menu_page(
            __('BR WP Marketo', 'brwpmarketo'),
            __('BR WP Marketo', 'brwpmarketo'),
            'manage_options',
            'brwpmarketo',
            array( $this, 'display_page' )
        );
    }

    public function display_page() {

    }

}