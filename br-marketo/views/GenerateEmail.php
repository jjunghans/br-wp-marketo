<?php

namespace BRWPMarketo\OptionsPage;

class GenerateEmail extends OptionsPage {

    public function __construct() {
        parent::__construct();

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'wp_ajax_get_email_template', array( $this, 'get_email_template' ) );
    }

    public function add_plugin_page() {
        add_submenu_page(
            'brwpmarketo',
            __( 'Create Email', 'brwpmarketo' ),
            __( 'Create Email', 'brwpmarketo' ),
            'manage_options',
            'brwpmarketo-generate-email',
            array( $this, 'display_page' )
        );
    }

    public function enqueue_scripts() {
        if( $_GET['page'] === 'brwpmarketo-generate-email' ) {
            wp_enqueue_script(
                'brwpmarketo-vue-js',
                BRMARKETO__PLUGIN_URL . 'scripts/vue.js',
                array(),
                '2.6.10',
                true
            );

            wp_enqueue_script(
                'brwpmarketo-generate-email',
                BRMARKETO__PLUGIN_URL . 'scripts/generate-email.js',
                array('underscore', 'brwpmarketo-vue-js', 'jquery-ui-accordion', 'jquery-ui-tabs'),
                '1.0.0',
                true
            );

            $opts = get_option('brwpmarketo_config');

            try {
                $templates = \BR\Marketo\EmailTemplate::getTemplates();

                wp_localize_script(
                    'brwpmarketo-generate-email',
                    'brwpmarketo_email',
                    array(
                        'marketo_templates' => $templates->result,
                        'wp_pages' => get_posts(array(
                            'posts_per_page' => -1,
                            'post_type' => array('post', 'page')
                        )),
                        'marketo_host' => $opts['host'],
                        'marketo_auth_token' => $opts['auth_token']
                    )
                );
            } catch (\BR\Marketo\MarketoException $e) {
                $this->errors[] = $e->getMessage();
            } catch (\BR\Marketo\CurlException $e) {
                $this->errors[] = $e->getMessage();
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
            }
        }
    }

    public function display_page() {
        ?>
        <div class="wrap">
            <h1><?php echo __( 'BR WP Marketo', 'brwpmarketo' ); ?></h1>
            <h2><?php echo __( 'Generate Email', 'brwpmarketo' ); ?></h2>

            <div id="nav-menus-frame">

                <div id="menu-settings-column" class="metabox-holder">

                    <div class="manage-menus">

                        <label>Select an email template</label>
                        <select @input="getTemplate">
                            <option value=""></option>
                            <option v-for="(template, idx) in templates" :key="idx" :value="template.id">{{template.folder.folderName}} / {{template.name}}</option>
                        </select>


                    </div>

                    <div v-if="selectedTemplate != null" id="side-sortables" class="accordion-container">

                        <ul class="outer-border">
                            <accordion-item context="pages" section-title="Pages">
                                <template v-slot:most-recent>
                                    <checklist post-type="page" :most-recent="true"></checklist>
                                </template>
                                <template v-slot:all>
                                    <checklist post-type="page"></checklist>
                                </template>
                            </accordion-item>
                            <accordion-item context="posts" section-title="Posts">
                                <template v-slot:most-recent>
                                    <checklist post-type="post" :most-recent="true"></checklist>
                                </template>
                                <template v-slot:all>
                                    <checklist post-type="post"></checklist>
                                </template>
                            </accordion-item>
                        </ul>
                    </div>

                </div>

                <div id="menu-management-liquid">
                    <div id="menu-management">

                            <div class="manage-menus">

                            </div>

                    </div>
                </div>
            </div>
        </div>

        <script type="text/x-template" id="template-accordion-item">
            <li class="control-section accordion-section ">
                <h3 class="accordion-section-title hndle" tabindex="0">
                    {{sectionTitle}} <span class="screen-reader-text">Press return or enter to open this section</span>
                </h3>
                <div class="accordion-section-content">
                    <div class="inside tabs">
                        <div class="posttypediv">
                            <ul class="add-menu-item-tabs">
                                <li class="tabs">
                                    <a class="nav-tab-link" :href="'#' + context + '-most-recent'">Most Recent</a>
                                </li>
                                <li>
                                    <a class="nav-tab-link" :href="'#' + context + '-all'">View All</a>
                                </li>
                                <li>
                                    <a class="nav-tab-link" :href="'#' + context + '-search'">Search</a>
                                </li>
                            </ul>

                            <div class="tabs-panel" :id="context + '-most-recent'">
                                <slot name="most-recent"></slot>
                            </div>
                            <div class="tabs-panel" :id="context + '-all'">
                                <slot name="all"></slot>
                            </div>
                            <div class="tabs-panel" :id="context + '-search'">
                                <slot name="search"></slot>
                            </div>
                        </div>
                    </div><!-- .inside -->
                </div><!-- .accordion-section-content -->
            </li><!-- .accordion-section -->
        </script>

        <script type="text/x-template" id="template-checklist">
            <ul class="categorychecklist form-no-clear">
                <li v-for="(item,idx) in items" :key="idx">
                    <label class="menu-item-title">
                        <input type="checkbox" class="menu-item-checkbox" :value="item.ID"> {{item.post_title}}
                    </label>
                </li>
            </ul>
        </script>
        <?php
    }


    public static function get_email_template() {

        $id = intval( $_GET['id'] );

        try {
            $template = \BR\Marketo\EmailTemplate::getByID( $id );
            echo json_encode( $template->result[0] );
        } catch( \BR\Marketo\MarketoException $e ) {
            return $e->getMessage();
        }
        wp_die();
    }

    public static function create_email_from_template() {

//        $id = intval( $_GET['template_id'] );

        try {
//            $template = \BR\Marketo\Email::createFromTemplate( $id );
//            echo json_encode( $template->result[0] );
        } catch( \BR\Marketo\MarketoException $e ) {
            return $e->getMessage();
        }
        wp_die();
    }

}