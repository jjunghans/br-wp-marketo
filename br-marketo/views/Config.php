<?php

namespace BRWPMarketo\OptionsPage;

use BRMarketo\Credentials;
use BRMarketo\RestClient;
use BRWPMarketo\Model\Email;

class Config extends OptionsPage {

    private $option_group = 'brwpmarketo';

    private $option_name = 'brwpmarketo_config';

    protected $available_templates = [];

    protected $available_folders = [];

    protected $options;

    public function __construct() {
        parent::__construct();

        if( isset( $_GET['page']) && $_GET['page'] == 'brwpmarketo-congif' ) {
            try {
                global $brWpMarketoRestClient;
                $this->available_templates = $brWpMarketoRestClient->getEmailTemplates();
                $this->available_folders = $brWpMarketoRestClient->getFolders(14);
            } catch (\Exception $e) {
                $this->errors[] = $e->getMessage();
            }
        }

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    public function add_plugin_page() {
        add_submenu_page(
            'brwpmarketo',
            __( 'Config', 'brwpmarketo' ),
            __( 'Config', 'brwpmarketo' ),
            'manage_options',
            'brwpmarketo-congif',
            array( $this, 'display_page' )
        );
    }

    public function page_init() {
        register_setting(
            $this->option_group,
            $this->option_name,
            array( $this, 'sanitize')
        );

        add_settings_section(
            'brwpmarketo-config', // ID
            'Config', // Title
            array( $this, 'print_intro' ),
            'brwpmarketo-config'
        );

        add_settings_field(
            'munchkin_id', // ID
            __( 'Munchkin ID', 'brwpmarketo' ),
            array( $this, 'print_input_munchkin_id' ),
            'brwpmarketo-config',
            'brwpmarketo-config' // Section,
        );

        add_settings_field(
            'client_id', // ID
            __( 'Client ID', 'brwpmarketo' ),
            array( $this, 'print_input_client_id' ),
            'brwpmarketo-config',
            'brwpmarketo-config' // Section
        );

        add_settings_field(
            'client_secret', // ID
            __( 'Client Secret', 'brwpmarketo' ),
            array( $this, 'print_input_client_secret' ),
            'brwpmarketo-config',
            'brwpmarketo-config' // Section
        );

        add_settings_field(
            'white_list_templates', // ID
            __( 'Templates', 'brwpmarketo' ),
            array( $this, 'print_available_templates' ),
            'brwpmarketo-config',
            'brwpmarketo-config' // Section,
        );

        add_settings_field(
            'white_list_folders', // ID
            __( 'Folders', 'brwpmarketo' ),
            array( $this, 'print_available_folders' ),
            'brwpmarketo-config',
            'brwpmarketo-config' // Section,
        );

    }

    public function display_page() {
        $this->options = get_option( 'brwpmarketo_config' );
        ?>
        <div class="wrap">
            <h1><?php echo __( 'BR WP Marketo', 'brwpmarketo' ); ?></h1>
            <form method="post" action="options.php">
                <?php new Email(); ?>
                <?php settings_fields( 'brwpmarketo' ); ?>
                <?php do_settings_sections( 'brwpmarketo-config' ); ?>
                <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    public function print_intro() {

    }

    public function print_input_munchkin_id() {
        $key = $this->option_name . '[munchkin_id]';
        $value = isset( $this->options['munchkin_id'] ) ? esc_attr( $this->options['munchkin_id'] ) : '';
        printf( '<input type="text" class="regular-text" id="munchkin_id" name="%s" value="%s" />', $key, $value );
    }

    public function print_input_client_id() {
        $key = $this->option_name . '[client_id]';
        $value = isset( $this->options['client_id'] ) ? esc_attr( $this->options['client_id'] ) : '';
        printf( '<input type="text" class="regular-text" id="client_id" name="%s" value="%s" />', $key, $value );
    }

    public function print_input_client_secret() {
        $key = $this->option_name . '[client_secret]';
        $value = isset( $this->options['client_secret'] ) ? esc_attr( $this->options['client_secret'] ) : '';
        printf( '<input type="text" class="regular-text" id="client_secret" name="%s" value="%s" />', $key, $value );
    }

    public function print_available_templates() {

        $key = $this->option_name . '[templates]';
        $wp_templates = get_page_templates();
        $selected_templates = isset( $this->options['templates'] ) ? $this->options['templates'] : [];

        ?>
        <table class="widefat fixed striped" cellpadding="0">
            <thead>
            <tr>
                <th scope="col" class="manange-column column-title column-primary" style="padding:8px 10px;font-weight:normal;">
                    <?php echo __( 'WP Template', BRMARKETO__TEXT_DOMAIN ); ?>
                </th>
                <th scope="col" class="manange-column" style="padding:8px 10px;font-weight:normal;">
                    <?php echo __( 'Marketo Template', BRMARKETO__TEXT_DOMAIN ); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if( count( $wp_templates ) < 1 || count( $this->available_templates ) < 1 ) { ?>
                <tr>
                    <td style="text-align:center;"><?php echo __( 'No available templates', BRMARKETO__TEXT_DOMAIN ); ?></td>
                </tr>
            <?php } else { ?>
                <?php foreach( $wp_templates as $wp_template => $file ) { ?>
                    <?php $selected = array_key_exists( $file, $selected_templates ) ? $selected_templates[$file] : null; ?>
                    <tr>
                        <td class="column-title">
                            <strong><?php echo $wp_template; ?></strong>
                        </td>
                        <td>
                            <select name="<?php echo $key; ?>[<?php echo $file; ?>]">
                                <option value=""><?php echo __( '-- Select a template', BRMARKETO__TEXT_DOMAIN ); ?></option>
                                <?php foreach( $this->available_templates as $template ) { ?>
                                <option value="<?php echo $template->getID(); ?>" <?php selected( $selected, $template->getID() ); ?>><?php echo $template->getName(); ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
        <?php
    }

    public function print_available_folders() {
        $key = $this->option_name . '[folder]';
        $selected_folder = isset( $this->options['folder'] ) ? $this->options['folder'] : null;
        ?>
        <select name="<?php echo $key; ?>" id="folder">
            <option value="">-- <?php echo __( 'Select a folder'); ?> --</option>
            <?php foreach( $this->available_folders as $folder ) { ?>
                <option value="<?php echo $folder->getID(); ?>" <?php selected( $selected_folder, $folder->getID() ); ?>><?php echo $folder->getName(); ?></option>
            <?php } ?>
        </select>
        <p class="description"><?php echo __( 'This is the folder in Marketo that generated emails will be saved in.', BRMARKETO__TEXT_DOMAIN ); ?></p>
        <?php
    }

    public function sanitize( $input ) {

        global $brWpMarketoRestClient;

        $sanitized = [];

        if( !isset( $input['munchkin_id'] ) || $input['munchkin_id'] == '' || $input['munchkin_id'] == null ) {
            $this->errors[] = 'Munchkin ID is required.';
        } else {
            $sanitized['munchkin_id'] = $input['munchkin_id'];
        }

        if( !isset( $input['client_id'] ) || $input['client_id'] == '' || $input['client_id'] == null ) {
            $this->errors[] = 'Client ID is required.';
        } else {
            $sanitized['client_id'] = $input['client_id'];
        }

        if( !isset( $input['client_secret'] ) || $input['client_secret'] == '' || $input['client_secret'] == null ) {
            $this->errors[] = 'Client Secret is required.';
        } else {
            $sanitized['client_secret'] = $input['client_secret'];
        }

        $sanitized['templates'] = [];
        if( isset( $input['templates'] ) ) {
            try {

                foreach( $input['templates'] as $wp_template => $marketo_template ) {
                    $tmpl_check = $brWpMarketoRestClient->getEmailTemplateByID( $marketo_template );
                    $sanitized['templates'][$wp_template] = $marketo_template;
                }
            } catch( \Exception $e ) {
                $this->errors[] = $e->getMessage();
            }
        }

        if( !isset( $input['folder'] ) || $input['folder'] == '' || $input['folder'] == null ) {
            $sanitized['folder'] = null;
        } else {
            $sanitized['folder'] = $input['folder'];
        }

        return $sanitized;
    }

}