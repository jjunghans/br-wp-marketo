<?php

namespace BR\Marketo;

/**
 * Class Config
 * @package Marketo
 * @description Gets/sets the config options for api connectivity
 */

class Config {

    /**
     * @var Config|null the created instance
     */
    private static $instance = null;

    protected $host;

    protected $client_id;

    protected $client_secret;

    protected $auth_token;

    /**
     * Config constructor.
     * Keep it private so we can make it a Singleton
     */
    private function __construct() {
        $opts = get_option('brwpmarketo_config');
        $this->host = isset( $opts['host'] ) ? $opts['host'] : '';
        $this->client_id = isset( $opts['client_id'] ) ? $opts['client_id'] : '';
        $this->client_secret = isset( $opts['client_secret'] ) ? $opts['client_secret'] : '';
        $this->auth_token = isset( $opts['auth_token' ] ) ? $opts['auth_token'] : '';
    }

    /**
     * prevent cloning of the object
     */
    private function __clone() {}
    private function __wakeup() {}
    private function __sleep() {}

    /**
     * The host to use for the API calls
     *
     * @return string
     */
    public function getHost():string {
        return $this->host;
    }

    /**
     * The client ID to use for the API authentication
     *
     * @return string
     */
    public function getClientID():string {
        return $this->client_id;
    }

    /**
     * The client secret to use for the API authentication
     *
     * @return string
     */
    public function getClientSecret():string {
        return $this->client_secret;
    }

    /**
     * The authentication token to use for the API calls
     *
     * @return string
     */
    public function getAuthToken() {
        return $this->auth_token;
    }

    /**
     * An instance of this class
     *
     * @return Config|null
     */
    public static function getInstance():Config {
        if ( self::$instance == null ) {
            self::$instance = new Config();
        }

        return self::$instance;
    }
}