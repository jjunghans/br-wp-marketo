<?php

namespace BRMarketo;

use GuzzleHttp\Client;
use BRMarketo\Model\Email;
use BRMarketo\Model\EditableSection;
use BRMarketo\Model\EmailTemplate;
use BRMarketo\Model\Folder;

/**
 * Class RestClient
 * @package BRMarketo
 */

class RestClient {

    private $client;

    /**
     * RestClient constructor.
     * @param Credentials $credentials
     * @throws \Exception
     */
    public function __construct( Credentials $credentials ) {

        $tokenData =  $credentials->getTokenData();

        $this->client = new Client([
            'base_uri' => $credentials->getApiBaseUrl(),
            'headers' => [
                'Authorization' => 'Bearer ' . $tokenData['access_token']
            ]
        ]);

    }

    /**
     * Sends the request and get the Response object
     * @param string $method
     *  - One of 'post' or 'get'
     * @param array $params
     * @return Response
     * @throws \Exception
     */
    private function request( string $endpoint, string $method, array $params = [] ):Response {
        $method_types = [ 'get', 'post' ];
        $method_idx = array_search( $method, $method_types );

        if( $method_idx === false ) {
            throw new \InvalidArgumentException('Invalid method' );
        }

        $sendParams = [
            $endpoint
        ];

        $method = $method_types[$method_idx];

        switch( $method ) {
            case 'get':
                $sendParams[]['query'] = $params;
                break;
            case 'post':
                $sendParams[]['form_params'] = $params;
                break;
        }

        // Perform the request
        $response = call_user_func_array( [ $this->client, $method ], $sendParams );

        // Get the body content of the request into an array
        $body = $response->getBody();
        $content = json_decode( $body->getContents(), true );

        // Build the response object
        return new Response( $content );
    }

    /**
     * EMAIL
     */

    /**
     * Retrieves an email by it's name
     * @param string $name
     *  - Name of the asset
     * @param array $folder
     *  - Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program' ,
     * @param string|null $status
     *  - Status filter for draft or approved versions
     * @return Email
     * @throws \Exception
     */
    public function getEmailByName( string $name, array $folder = [], string $status = null ):Email {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/byName.json';

        // The parameters to send with the request
        $query = [
            'name' => $name
        ];

        // The possible types for folders
        $folder_types = [ 'Program', 'Folder' ];

        // Add to the query if a folder is given
        if( is_array( $folder ) && isset( $folder['id'] ) && isset( $folder['type'] ) ) {
            if( !in_array( $folder['type'], $folder_types ) ) {
                throw new \Exception( 'Invalid folder type' );
            }

            $query['folder'] = json_encode( $folder );
        }

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search($status, $status_types);

            if ($status_idx !== false) {
                $query['status'] = $status_types[$status_idx];
            }
        }

        $response = $this->request( $endpointUrl, 'get', $query );

        // return a object model
        return new Email( $response->getResult()[0] );
    }

    /**
     * Retrieves an email by it's ID
     * @param int $id
     *  -  Id of the asset
     * @param string|null $status
     *  - Status filter for draft or approved versions
     * @return Email
     * @throws \Exception
     */
    public function getEmailByID( int $id, string $status = null ):Email {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$id.'.json';

        // the parameters to send
        $sendParams = [];

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search($status, $status_types);

            if ($status_idx !== false) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // return a object model
        return new Email( $response->getResult()[0] );
    }

    /**
     * Retrieves an array of emails
     * @param array $folder
     *  - Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program' ,
     * @param null $status
     *  - Status filter for draft or approved versions
     * @param int|null $offset
     *  - An integer that can be used with maxReturn to read through large result sets
     * @param int $maxReturn
     *  - An integer that limits the number of results (default is 20, maximum is 200)
     * @return array
     * @throws \Exception
     */
    public function getEmails( array $folder = [], $status = null, int $offset = 0, int $maxReturn = 20 ):array {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emails.json';

        // The parameters to send with the request
        $sendParams = [
            'maxReturn' => $maxReturn,
            'offset' => $offset
        ];

        // Add to the query if a folder is given
        if( is_array( $folder ) && isset( $folder['id'] ) && isset( $folder['type'] ) ) {

            // The possible types for folders
            $folder_types = [ 'Program', 'Folder' ];

            if( !in_array( $folder['type'], $folder_types ) ) {
                throw new \Exception( 'Invalid folder type' );
            }

            $sendParams['folder'] = json_encode( $folder );
        }

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search( $status, $status_types );

            // Add to the query is a status is given
            if ($status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // Map result array to Email objects
        $emails = $response->getResult();
        array_walk( $emails, function(&$v) {
            $v = new Email($v);
        });

        // return results
        return $emails;
    }

    /**
     * Creates an email
     * @param int $template
     *  - Id of the parent template
     * @param array $folder
     *  - Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program'
     * @param string $name
     *  - Name of the asset
     * @param array $optional
     *  -   Optional values to apply to the email
     *  -   @param string $description
     *          - Description of the asset
     *  -   @param string|null $subject
     *          - Subject Line of the Email
     *  -   @param string|null $fromName
     *          -  From-name of the Email
     *  -   @param string|null $fromEmail
     *          -  From-address of the Email
     *  -   @param string|null $replyEmail
     *          - Reply-To address of the Email
     *  -   @param bool $operational
     *          - Whether the email is operational. Operational emails bypass unsubscribe status. Defaults to false
     * @return Email
     * @throws \Exception
     */
    public function createEmail( int $template, array $folder, string $name, array $optional = [] ):Email {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emails.json';

        // The possible types for folders
        $folder_types = [ 'Program', 'Folder' ];
        $encoded_folder = null;

        // Add to the query if a folder is given
        if( is_array( $folder ) && isset( $folder['id'] ) && isset( $folder['type'] ) ) {
            if( !in_array( $folder['type'], $folder_types ) ) {
                throw new \Exception( 'Invalid folder type' );
            }

            $encoded_folder = json_encode( $folder );
        }

        // The parameters to send with the request
        $sendParams = [
            'template' => $template,
            'folder' => $encoded_folder,
            'name' => $name
        ];

        // whitelist the optional value keys
        $available_options = [ 'description', 'subject', 'fromName', 'fromEmail', 'replyEmail', 'operational' ];

        // add the optional values to the send params
        foreach( $available_options as $opt ) {
            if( array_key_exists( $opt, $optional ) ) {
                switch( $opt ) {
                    case 'operational':
                        if( is_bool( $optional[$opt] ) ) {
                            $sendParams[$opt] = $optional[$opt];
                        }
                        break;
                    default:
                        $sendParams[$opt] = $optional[$opt];
                }

            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        // return a object model
        return new Email( $response->getResult()[0] );
    }

    /**
     * Updates an email's metadata
     * @param int $id
     *  -   Id of the asset
     * @param array $props
     *  -   Optional values to apply to the email
     *  -   @param string $description
     *          - Description of the asset
     *  -   @param string $name
     *          - Name of the asset
     *  -   @param bool $operational
     *          - Whether the email is operational. Operational emails bypass unsubscribe status. Defaults to false
     *  -   @param bool $published
     *          - Whether the email has been published to Sales Insight
     *  -   @param bool $textOnly
     *          - Setting to include text-only version of email when sent
     *  -   @param bool $webView
     *          - Whether the email has been enabled to allow the 'View as Web Page' when received
     * @return Email
     * @throws \Exception
     */
    public function updateEmail( int $id, array $props = [] ):Email {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$id.'.json';

        // the parameters to send
        $sendParams = [];

        $available_props = [ 'description', 'name', 'operational', 'published', 'textOnly', 'webView' ];

        foreach( $available_props as $prop ) {
            if( isset( $props[$prop] ) ) {
                switch( $prop ) {
                    case 'operational':
                    case 'published':
                    case 'textOnly':
                    case 'webView':
                        $sendParams[$prop] = !is_bool( $props[$prop] ) ? false : $props[$prop];
                        break;
                    default:
                        $sendParams[$prop] = $props[$prop];
                }
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        // return a object model
        return new Email( $response->getResult()[0] );
    }

    /**
     * Updates the content for an email
     * @param int $id
     *  -   Id of the asset
     * @param array $props
     *  -   Optional values to apply to the email
     *  -   @param string $fromName
     *          - From-name of the Email
     *  -   @param bool $fromEmail
     *          - From-address of the Email
     *  -   @param bool $replyTo
     *          - Reply-To address of the Email
     *  -   @param bool $subject
     *          - Subject Line of the Email
     * @return bool
     * @throws \Exception
     */
    public function updateEmailContent( int $id, array $props = [] ):bool {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$id.'/content.json';

        // the parameters to send
        $sendParams = [];

        $available_props = [ 'fromName', 'fromEmail', 'replyTo', 'subject' ];

        foreach( $available_props as $prop ) {
            if( isset( $props[$prop] ) ) {
                $sendParams[$prop] = $props[$prop];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        return $response->isSuccess();
    }

    /**
     * Sets the status of an email to 'approved'
     * @param int $id
     *  -   The ID of the asset
     * @return bool
     * @throws \Exception
     */
    public function approveEmail( int $id ):bool {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$id.'/approveDraft.json';

        // Perform the request
        $response = $this->request( $endpointUrl, 'post' );

        // Throw error if it didn't work
        if( !$response->isSuccess() ) {
            throw new \Exception( $response->getError() );
        }

        return true;
    }

    /**
     * Sets the status of an email to 'draft'
     * @param int $id
     *  -   ID of the email
     * @return bool
     * @throws \Exception
     */
    public function unapproveEmail( int $id ):bool {
        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$id.'/unapprove.json';

        // Perform the request
        $response = $this->request( $endpointUrl, 'post' );

        // Throw error if it didn't work
        if( !$response->isSuccess() ) {
            throw new \Exception( $response->getError() );
        }

        return true;
    }

    /**
     * Retrieves an array of editable sections in an email
     * @param int $emailID
     *  -  Id of the asset
     * @param null $status
     *  -   Status filter for draft or approved versions
     * @return array
     * @throws \Exception
     */
    public function getEditableSections( int $emailID, $status = null ):array {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$emailID.'/content.json';

        // The parameters to send with the request
        $sendParams = [];

        // Add to the query is a status is given
        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = [ 'approved', 'draft' ];
            $status_idx = array_search( $status, $status_types );

            if( $status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // Map result array to EmailContent objects
        $contents = $response->getResult();
        array_walk( $contents, function(&$v) {
            $v = new EditableSection($v);
        });

        // return results
        return $contents;
    }

    /**
     * @param int $emailID
     *  -   Id of the email
     * @param null $status
     *  -   Status filter for draft or approved versions
     * @param int $leadID
     *  -   The lead id to impersonate
     * @param string $type
     *  -   Email content type to return
     * @return string
     * @throws \Exception
     */
    public function getFullContent( int $emailID, $status = null, int $leadID = 0, string $type = 'HTML' ):string {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$emailID.'/fullContent.json';

        // The parameters to send with the request
        $sendParams = [];

        // Add to the query is a status is given
        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = [ 'approved', 'draft' ];
            $status_idx = array_search( $status, $status_types );

            if( $status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        if( $leadID > 0 ) {
            $sendParams['leadId'] = $leadID;
        }

        $types = [ 'HTML', 'Text' ];
        $type_idx = array_search( $type, $types );

        if( $type_idx !== false ) {
            $sendParams['type'] = $types[$type_idx];
        }

        $response = $this->request( $endpointUrl, 'get', $sendParams );

        return $response->getResult()[0]['content'];
    }

    /**
     * Updates an editable section of an email
     * @param int $emailID
     *  - Id of the email
     * @param string $htmlID
     *  - HTML ID of the content section
     * @param string $type
     *  - Type of content to set for the section. = ['Text', 'DynamicContent', 'Snippet']
     * @param string $value
     *  - Value to set for the section. For type Text, the HTML content of the section. For type DynamicContent, the id of the segmentation to use for the content. For type Snippet, the id of the snippet to embed
     * @param array $optional
     *  - Additional properties to apply to the content section
     *  -   @param string altText
     *          - Sets the value of the alt parameter for the resulting img element
     *  -   @param string externalUrl
     *  -   @param int height
     *          - Overrides native height of the image. The resulting file will be resized to the given height
     *  -   @param string image
     *          - Multipart file that allows you to load an image from your computer
     *  -   @param string linkUrl
     *  -   @param bool overWrite
     *          - Allows overwriting of the existing image content section
     *  -   @param string style
     *          - Sets the value of the style parameter for the content section
     *  -   @param string textValue
     *  -   @param string videoUrl
     *          - Sets the Url of the video element. Videos must be either from YouTube or Vimeo
     *  -   @param int width
     *          - Overrides native width of the image. The resulting file will be resized to the given width
     * @return bool
     * @throws \Exception
     */
    public function setEditableSection( int $emailID, string $htmlID, string $type, string $value, array $optional = [] ):bool {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$emailID.'/content/'.$htmlID.'.json';

        // The parameters to send with the request
        $sendParams = [];

        // whitelist the available props
        $available_props = [ 'altText', 'externalUrl', 'height', 'image', 'linkUrl', 'overWrite', 'style', 'textValue', 'videoUrl', 'width' ];

        // whitelist the available types
        $available_types = [ 'Text', 'DynamicContent', 'Snippet' ];

        // find the given type in the available types
        $found_type_idx = array_search( $type, $available_types );

        // The given type was not found
        if( $found_type_idx === false ) {
            throw new \InvalidArgumentException('Invalid type' );
        }

        // Add the type and value to the send params
        $sendParams['type'] = $available_types[$found_type_idx];
        $sendParams['value'] = $value;

        // Add the optional props to the send params
        foreach( $available_props as $prop ) {
            if( array_key_exists( $prop, $optional ) ) {
                $sendParams[$prop] = $optional[$prop];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        if( !$response->isSuccess() ) {
            throw new \Exception( $response->getError() );
        }

        return true;
    }

    /**
     * Sends a sample of the email
     * @param int $emailID
     *  -   ID of the email
     * @param string $to
     *  -   Email address to receive sample email
     * @param int $leadID
     *  -   Id of a lead to impersonate
     * @param bool $textOnly
     *  -   Whether to send to text only version along with the HTML version
     * @return bool
     * @throws \Exception
     */
    public function sendSampleEmail( int $emailID, string $to, int $leadID = 0, bool $textOnly = false ):bool {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/email/'.$emailID.'/sendSample.json';

        // The parameters to send with the request
        $sendParams = [
            'emailAddress' => $to,
            'textOnly' => $textOnly
        ];

        if( $leadID > 0 ) {
            $sendParams['leadId'] = $leadID;
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        if( !$response->isSuccess() ) {
            throw new \Exception( $response->getError() );
        }

        return true;
    }

    /**
     * EMAIL TEMPLATE
     */

    /**
     * Retrieves an email template by it's ID
     * @param int $id
     *  -   ID of the asset
     * @param string|null $status
     *  -   Status filter for draft or approved versions
     * @return EmailTemplate
     * @throws \Exception
     */
    public function getEmailTemplateByID( int $id, string $status = null ) {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emailTemplate/'.$id.'.json';

        // The parameters to send with the request
        $sendParams = [];

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search( $status, $status_types );

            // Add to the query is a status is given
            if($status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // return a object model
        return new EmailTemplate( $response->getResult()[0] );

    }

    /**
     * Retrieves an email template by it's name
     * @param string $name
     *  -   ID of the asset
     * @param string|null $status
     *  -   Status filter for draft or approved versions
     * @return EmailTemplate
     * @throws \Exception
     */
    public function getEmailTemplateByName( string $name, string $status = null ) {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emailTemplate/byName.json';

        // The parameters to send with the request
        $sendParams = [
            'name' => $name
        ];

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search( $status, $status_types );

            // Add to the query is a status is given
            if ($status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // return a object model
        return new EmailTemplate( $response->getResult()[0] );

    }

    /**
     * Retrieves an array of email templates
     * @param null $status
     *  - Status filter for draft or approved versions
     * @param int|null $offset
     *  - An integer that can be used with maxReturn to read through large result sets
     * @param int $maxReturn
     *  - An integer that limits the number of results (default is 20, maximum is 200)
     * @return array
     * @throws \Exception
     */
    public function getEmailTemplates( $status = null, int $offset = 0, int $maxReturn = 20 ):array {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emailTemplates.json';

        // The parameters to send with the request
        $sendParams = [
            'maxReturn' => $maxReturn,
            'offset' => $offset
        ];

        if( !is_null( $status ) ) {
            // The possible statuses
            $status_types = ['approved', 'draft'];
            $status_idx = array_search( $status, $status_types );

            // Add to the query is a status is given
            if ($status_idx !== false ) {
                $sendParams['status'] = $status_types[$status_idx];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // Map result array to EmailTemplate objects
        $email_templates = $response->getResult();
        array_walk( $email_templates, function(&$v) {
            $v = new EmailTemplate($v);
        });

        // return results
        return $email_templates;
    }

    /**
     * Update the metadata for an email template
     * @param int $id
     *  -   Id of the asset
     * @param string|null $name
     *  -   Name of the Email Template
     * @param string|null $description
     *  -   Description of the asset
     * @return EmailTemplate
     * @throws \Exception
     */
    public function updateEmailTemplate( int $id, string $name = null, string $description = null ):EmailTemplate {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/emailTemplate/'.$id.'.json';

        // The parameters to send with the request
        $sendParams = [];

        if( !is_null( $name ) ) {
            $sendParams['name'] = $name;
        }

        if( !is_null( $description ) ) {
            $sendParams['description'] = $description;
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'post', $sendParams );

        if( !$response->isSuccess() ) {
            throw new \Exception( $response->getError() );
        }

        return new EmailTemplate( $response->getResult()[0] );
    }

    /**
     * FOLDER
     */

    /**
     * Retrieves a folder by it's ID
     * @param int $id
     *  -   ID of the asset
     * @param string|null $type
     *  -   Type of folder. 'Folder' or 'Program'
     * @return Folder
     * @throws \Exception
     */
    public function getFolderByID( int $id, string $type = null ):Folder {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/folder/'.$id.'.json';

        // The parameters to send with the request
        $sendParams = [];

        if( !is_null( $type ) ) {
            // The possible statuses
            $type_types = ['Folder', 'Program'];
            $type_idx = array_search( $type, $type_types );

            // Add to the query is a status is given
            if ($type_idx !== false ) {
                $sendParams['type'] = $type_types[$type_idx];
            }
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // return a object model
        return new Folder( $response->getResult()[0] );

    }

    /**
     * Retrieves a folder by it's name
     * @param string $name
     *  -   Name of the asset
     * @param string|null $type
     *  -   Type of folder. 'Folder' or 'Program'
     * @param string $root
     *  -   Parent folder reference
     * @param string $workspace
     *  -   Name of the workspace
     * @return Folder
     * @throws \Exception
     */
    public function getFolderByName( string $name, string $type = null, string $root = null, string $workspace = null ):Folder {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/folder/byName.json';

        // The parameters to send with the request
        $sendParams = [
            'name' => $name
        ];

        if( !is_null( $type ) ) {
            // The possible statuses
            $folder_types = ['Folder', 'Program'];
            $type_idx = array_search( $type, $folder_types );

            // Add to the query is a status is given
            if($type_idx !== false ) {
                $sendParams['type'] = $folder_types[$type_idx];
            }
        }

        if( !is_null( $root ) ) {
            $sendParams['root'] = $root;
        }

        if( !is_null( $workspace ) ) {
            $sendParams['workspace'] = $workspace;
        }

        // Perform the request
        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // return a object model
        return new Folder( $response->getResult()[0] );

    }

    /**
     * Retrieves an array of folders
     * @param string|null $root
     *  -   Parent folder reference
     * @param int $maxDepth
     *  -   Maximum folder depth to traverse
     * @param int $maxReturn
     *  -   Maximum number of folders to return
     * @param int $offset
     *  -   Integer offset for paging
     * @param string|null $workspace
     *  -   Name of the workspace
     * @return array
     * @throws \Exception
     */
    public function getFolders( string $root = null, int $maxDepth = 2, int $maxReturn = 20, int $offset = 0, string $workspace = null ):array {

        // The endpoint to hit
        $endpointUrl = '/rest/asset/v1/folders.json';

        // The parameters to send with the request
        $sendParams = [
            'maxDepth' => $maxDepth,
            'maxReturn' => $maxReturn,
            'offset' => $offset
        ];

        if( !is_null( $root ) ) {
            $sendParams['root'] = $root;
        }

        if( !is_null( $workspace ) ) {
            $sendParams['workspace'] = $workspace;
        }

        $response = $this->request( $endpointUrl, 'get', $sendParams );

        // Map result array to Folder objects
        $folders = $response->getResult();
        array_walk( $folders, function(&$v) {
            $v = new Folder($v);
        });

        // return results
        return $folders;
    }
}