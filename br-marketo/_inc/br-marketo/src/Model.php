<?php

namespace BRMarketo;

class Model {

    protected $data;

    public function __construct( $data = [] ) {
        if( !is_array( $data ) ) {
            throw new \Exception('No result found' );
        }
        $this->data = $data;
    }

}