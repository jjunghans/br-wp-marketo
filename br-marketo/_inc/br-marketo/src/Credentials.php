<?php

namespace BRMarketo;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Class Credentials
 * @package BRMarketo
 */

class Credentials {

    /**
     * @var string
     */
    private $munchkin_id;

    /**
     * @var string
     */
    private $client_id;

    /**
     * @var string
     */
    private $client_secret;

    /**
     * Credentials constructor.
     * @param string $munchkin_id
     * @param string $client_id
     * @param string $client_secret
     */
    public function __construct( string $munchkin_id, string $client_id, string $client_secret ) {
        $this->munchkin_id = $munchkin_id;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    public function getTokenData():array {
        $client = new GuzzleClient( [ 'base_uri' => $this->getApiBaseUrl() ] );

        $request = $client->get( '/identity/oauth/token', [
            'query' => [
                'grant_type' => 'client_credentials',
                'client_id' => '1d9edc92-3a48-465d-a94e-470e730cc8b3',
                'client_secret' => $this->client_secret
            ]
        ]);

        $body = $request->getBody();
        $content = json_decode( $body->getContents(), true );

        if( !isset($content['access_token'] ) ) {
            throw new \Exception( 'Invalid Marketo credentials' );
        }

        return $content;

    }

    public function getApiBaseUrl() {
        return 'https://' . $this->munchkin_id . '.mktorest.com';
    }

}