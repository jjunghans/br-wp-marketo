<?php

namespace BRMarketo;

/**
 * Class Response
 * @package BRMarketo
 */

class Response {

    /**
     * @var
     */
    protected $data;

    /**
     * Response constructor.
     * @param array $data
     */
    public function __construct( array $data ) {
        $this->data = $data;

        if( !$this->isSuccess() ) {
            throw new \Exception( $this->getError() );
        }
    }

    /**
     * @return mixed|null
     */
    public function getResult() {
        return isset( $this->data['result'] ) ? $this->data['result'] : null;
    }

    /**
     * @return mixed
     */
    public function getRequestID() {
        return $this->data['requestID'];
    }

    /**
     * @return mixed|null
     */
    public function getNextPageToken() {
        return isset( $this->data['nextPageToken'] ) ? $this->data['nextPageToken'] : null;
    }

    /**
     * @return bool
     */
    public function isSuccess() {
        return true === $this->data['success'];
    }

    public function getWarning() {
        return isset( $this->data['warnings'] ) && count( $this->data['warnings'] ) ? $this->data['warnings'][0] : null;
    }

    /**
     * @return |null
     */
    public function getError() {
        return isset( $this->data['errors'] ) && count( $this->data['errors'] ) ? $this->data['errors'][0]['message'] : null;
    }
}