<?php

namespace BRMarketo\Model;

use BRMarketo\Model;
use DateTime;

/**
 * Class Email
 * @package BRMarketo\Response
 */

class Email extends Model {

    /**
     * Id of the asset
     * @return int
     */
    public function getID() {
        return (int) $this->data['id'];
    }

    /**
     * Name of the asset
     * @return string
     */
    public function getName() {
        return (string) $this->data['name'];
    }

    /**
     * Description of the asset
     * @return string
     */
    public function getDescription() {
        return (string) array_key_exists( 'description', $this->data ) ? $this->data['description'] : '';
    }

    /**
     * Datetime the asset was created
     * @return DateTime
     * @throws \Exception
     */
    public function getCreatedAt() {
        return new DateTime( $this->data['createdAt'] );
    }

    /**
     * Datetime the asset was most recently updated
     * @return DateTime
     * @throws \Exception
     */
    public function getUpdatedAt() {
        return new DateTime( $this->data['updatedAt'] );
    }

    /**
     * Subject Line of the Email
     * @return array
     */
    public function getSubject() {
        return (array) $this->data['subject'];
    }

    /**
     * From-name of the Email
     * @return array
     */
    public function getFromName() {
        return (array) $this->data['fromName'];
    }

    /**
     * From-address of the Email
     * @return array
     */
    public function getFromEmail() {
        return (array) $this->data['fromEmail'];
    }

    /**
     * Reply-To address of the Email
     * @return array|null
     */
    public function getReplyEmail() {
        return (array) $this->data['replyEmail'];
    }

    /**
     * Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program'
     * @return array
     */
    public function getFolder() {
        return (array) $this->data['folder'];
    }

    /**
     * Whether the email is operational. Operational emails bypass unsubscribe status.
     * @return bool
     */
    public function isOperational() {
        return true === $this->data['operational'];
    }

    /**
     * Setting to include text-only version of email when sent
     * @return bool
     */
    public function isTextOnly() {
        return true === $this->data['textOnly'];
    }

    /**
     * Whether the email is published to Marketo Sales Insight
     * @return bool
     */
    public function isPublishedToMSI() {
        return true === $this->data['publishToMSI'];
    }

    /**
     * Status filter for draft or approved versions
     * @return string
     */
    public function getStatus() {
        return (string) $this->data['status'];
    }

    /**
     *  Id of the parent template
     * @return int
     */
    public function getTemplate() {
        return (int) $this->data['template'];
    }

    /**
     * Whether 'View as Webpage' function is enabled for the email
     * @return bool
     */
    public function hasWebView() {
        return true === $this->data['webView'];
    }

    /**
     * Name of the workspace
     * @return string
     */
    public function getWorkspace() {
        return (string) $this->data['workspace'];
    }

    /**
     * The type version of the email = ['1', '2']
     * @return int
     */
    public function getVersion() {
        return (int) $this->data['version'];
    }
}