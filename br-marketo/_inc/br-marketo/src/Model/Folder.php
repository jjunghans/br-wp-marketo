<?php

namespace BRMarketo\Model;

use BRMarketo\Model;
use DateTime;

class Folder extends Model {

    /**
     * Id of the asset
     * @return int
     */
    public function getID() {
        return (int) $this->data['id'];
    }
    /**
     * Folder Id of the asset
     * @return int
     */
    public function getFolderID() {
        return (array) $this->data['folderId'];
    }

    /**
     * Type of folder
     * @return string
     */
    public function getFolderType() {
        return (string) $this->data['folderType'];
    }

    /**
     * Name of the asset
     * @return string
     */
    public function getName() {
        return (string) $this->data['name'];
    }

    /**
     * Description of the asset
     * @return string
     */
    public function getDescription() {
        return (string) $this->data['description'];
    }

    /**
     * Datetime the asset was created
     * @return DateTime
     * @throws \Exception
     */
    public function getCreatedAt() {
        return new DateTime( $this->data['createdAt'] );
    }

    /**
     * Datetime the asset was most recently updated
     * @return DateTime
     * @throws \Exception
     */
    public function getUpdatedAt() {
        return new DateTime( $this->data['updatedAt'] );
    }

    /**
     * The access zone ID
     * @return int
     */
    public function getAccessZoneID() {
        return (int) $this->data['accessZoneId'];
    }

    public function isArchive() {
        return true === $this->data['isArchive'];
    }

    public function isSystem() {
        return true === $this->data['isSystem'];
    }

    public function getParent() {
        return (array) $this->data['parent'];
    }

    public function getPath() {
        return (string) $this->data['path'];
    }

    public function getUrl() {
        return (string) $this->data['url'];
    }

    public function getWorkspace() {
        return (string) $this->data['workspace'];
    }

}