<?php

namespace BRMarketo\Model;

use BRMarketo\Model;

class EditableSection extends Model {

    public function getHtmlID() {
        return $this->data['htmlId'];
    }

    public function getContentType() {
        return $this->data['contentType'];
    }

    public function getIndex() {
        return isset( $this->data['index'] ) ? $this->data['index'] : null;
    }

    public function getParentHtmlID() {
        return $this->data['parentHtmlId'];
    }

    public function isLocked() {
        return true === $this->data['isLocked'];
    }

    public function getValue() {
        return isset( $this->data['value'] ) ? $this->data['value'] : null;
    }

}