<?php

namespace BRMarketo\Model;

use BRMarketo\Model;
use DateTime;

class EmailTemplate extends Model {

    /**
     * Id of the asset
     * @return int
     */
    public function getID() {
        return (int) $this->data['id'];
    }

    /**
     * Name of the asset
     * @return string
     */
    public function getName() {
        return (string) $this->data['name'];
    }

    /**
     * Description of the asset
     * @return string
     */
    public function getDescription() {
        return (string) $this->data['description'];
    }

    /**
     * Datetime the asset was created
     * @return DateTime
     * @throws \Exception
     */
    public function getCreatedAt() {
        return new DateTime( $this->data['createdAt'] );
    }

    /**
     * Datetime the asset was most recently updated
     * @return DateTime
     * @throws \Exception
     */
    public function getUpdatedAt() {
        return new DateTime( $this->data['updatedAt'] );
    }


    /**
     * Url of the asset in the Marketo UI
     * @return string
     */
    public function getUrl() {
        return (string) $this->data['url'];
    }

    /**
     * Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program'
     * @return array
     */
    public function getFolder() {
        return (array) $this->data['folder'];
    }

    /**
     * Name of the workspace
     * @return string
     */
    public function getWorkspace() {
        return (string) $this->data['workspace'];
    }

    /**
     * The type version of the email = ['1', '2']
     * @return int
     */
    public function getVersion() {
        return (int) $this->data['version'];
    }

}