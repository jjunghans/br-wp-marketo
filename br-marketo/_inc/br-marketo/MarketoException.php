<?php

namespace BR\Marketo;

use \Exception;

/**
 * Class MarketoException
 * @package Marketo
 * @description Base class for Exception handling
 */

class MarketoException extends Exception {}