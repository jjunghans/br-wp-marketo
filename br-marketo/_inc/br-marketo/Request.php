<?php

namespace BR\Marketo;

/**
 * Class Request
 * @package Marketo
 * @description Performs cURL requests for the Marketo API
 */

class Request {

    /**
     * Creates the URL string to make the API request
     * @param string $url
     * @param array $params
     * @return string
     */
    private static function __buildUrl( string $url, array $params = [] ):string {

        // Bring in the config instance
        $config = Config::getInstance();

        // Build the URL to hit
        $url = $config->getHost() . $url . '?' . http_build_query( $params );

        return $url;
    }

    /**
     * Executes the cURL request
     * @param string $url
     * @param array $url_params
     * @param array $params
     * @return Response
     * @throws CurlException
     * @throws MarketoException
     */
    public static function _call( string $url, array $url_params = [], array $params = [] ):Response {

        // Initialize cURL
        $ch = curl_init( self::__buildUrl( $url, $url_params ) );

        // Set the cURL call options
        curl_setopt( $ch,  CURLOPT_RETURNTRANSFER, 1 );

        // If $parameters are passed, assume this is a POST request and put the passed parameters into play
        if( is_array( $params ) && !empty( $params ) ) {
            curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'accept: application/x-www-form-urlencoded' ] );
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        } else {
            curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'accept: application/json' ] );
        }

        // Execute the cURL call
        $response = curl_exec( $ch );

        // If an error happens, kill further processing and perform necessary logging
        if( curl_errno( $ch ) ) {

            // The cURL error message
            $error = curl_error( $ch );

            // Close the cURL reference
            curl_close( $ch );

            // Throw the exception
            throw new CurlException( $error );
        }

        // Close the cURL reference
        curl_close( $ch );

        if( (!property_exists( 'success', $response) || !$response->success) ) {
            $decoded_response = json_decode( $response );

            if( property_exists('errors', $response ) && is_array( $decoded_response->errors ) && count( $decoded_response->errors ) > 0 ) {
                throw new MarketoException($decoded_response->errors[0]->message, $decoded_response->errors[0]->code);
            }
        }

        // The response
        return new Response( $response );
    }

}