<?php

namespace BR\Marketo;

class Folder {

    public static function getAll() {
        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/folders.json',
            [
                'access_token' => $config->getAuthToken(),
                'root' => json_encode(['id' => 14, 'type' => 'Folder']),
                'maxDepth' => 1
            ]
        );

    }

}