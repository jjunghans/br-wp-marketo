<?php

namespace BR\Marketo;

/**
 * Class Response
 * @package Marketo
 * @description A wrapper class for the response from the API
 */

class Response {

    /**
     * @var array An array representation of the response from the API
     */
    protected $_response = array();

    /**
     * Response constructor. Sets the $_response property
     * @param array|string $response The JSON string or an array representing the JSON string returned from the API
     * @throws MarketoException
     */
    public function __construct( $data = null ) {
        $r = $data;

        if( !is_array( $r ) ) {
            // It could be a JSON string
            $decoded = json_decode( $r, true );

            // If $decoded is null, there was a problem parsing into an array
            if( is_null( $decoded ) ) {
                throw new MarketoException('The response could not be converted into an array.  The JSON response may be malformed.');
            }

            $r = $decoded;
        }

        $this->_response = $r;
    }

    /**
     * Returns the value at the specified key in $_response
     * @param string|null $key The key to retrieve from the response
     * @return array|mixed|null
     * @throws MarketoException
     */
    public function __get( $key = null ) {
        if( !array_key_exists( $key, $this->_response ) ) {
            throw new MarketoException('Key \''.$key.'\' does not exist ');
        }

        $return = $this->_response[$key];

        return $return;
    }

}