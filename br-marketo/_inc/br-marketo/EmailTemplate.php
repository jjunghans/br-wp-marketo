<?php

namespace BR\Marketo;

/**
 * Class EmailTemplate
 * @package Marketo
 * @description Handles interactions for Email templates via the API
 * @link http://developers.marketo.com/rest-api/assets/email-templates/
 */

class EmailTemplate {

    /**
     * Gathers the email templates from the API
     *
     * @return Response
     *
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/email-templates/#browse
     */
    public static function getTemplates():Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/emailTemplates.json',
            [
                'access_token' => $config->getAuthToken()
            ]
        );
    }

    /**
     * Gathers the data for a template with the given ID
     *
     * @param $id
     *      ID of the template to get
     *
     * @return array
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/email-templates/#by_id
     */
    public static function getByID( $id ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/emailTemplate/'.$id.'.json',
            [
                'access_token' => $config->getAuthToken()
            ]
        );
    }

    /**
     * Gathers the data for a template with the given ID
     *
     * @param string $name
     *      Name of the template to get
     *
     * @return Response
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/email-templates/#by_id
     */
    public static function getByName( string $name ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/emailTemplate/byName.json',
            [
                'access_token' => $config->getAuthToken(),
                'name' => $name
            ]
        );
    }
}