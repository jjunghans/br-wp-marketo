<?php

namespace BR\Marketo;

/**
 * Class File
 * @package Marketo
 * @description Handles file management
 * @link http://developers.marketo.com/rest-api/assets/files/
 */

class File {

    /**
     * Upload a file to Marketo
     *
     * @param string $name
     *      Name to give the file
     * @param array $folder
     *      Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program'
     * @param string $file
     *      The file data
     * @param string $description
     *      Description of the file
     *
     * @return Response
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/files/#create_and_update
     */
    public static function upload( string $name, array $folder, string $file, string $description = '' ):Response {
        // Check that the file to add to Marketo exists
        if( !file_exists( $file ) ) {
            throw new MarketoException('File not found');
        }

        // Bring in the config settings
        $config = Config::getInstance();

        // Perform the request
        return Request::_call(
            '/rest/asset/v1/files.json',
            [
                'access_token' => $config->getAuthToken()
            ],
            [
                'name' => $name,
                'folder' => json_encode( $folder ),
                'file' => new \CURLFile( $file, mime_content_type( $file ), basename( $file ) ),
                'description' => $description
            ]
        );
    }

    /**
     * Gathers the file's data from the API
     *
     * @param int $id
     *      ID of the file to get
     *
     * @return array
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @link http://developers.marketo.com/rest-api/assets/files/#by_id
     */
    public function getByID( int $id ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/file/'.$id.'.json',
            [
                'access_token' =>  $config->getAuthToken()
            ]
        );
    }

}