<?php

namespace BR\Marketo;

/**
 * Class OAuth
 * @package Marketo
 * @description Handles OAuth interactivity
 * @link http://developers.marketo.com/rest-api/authentication/
 */

class OAuth {

    /**
     * The grant type to assign to the authentication token
     */
    const GRANT_TYPE = 'client_credentials';

    /**
     * @return string The authentication token requested from the API
     * @throws CurlException
     * @throws MarketoException
     * @see Request::_call()
     * @link http://developers.marketo.com/rest-api/authentication/#creating_an_access_token
     */
    public static function requestAuthToken() {

        // Bring in the config settings
        $config = Config::getInstance();

        // Perform the call to the API
        $response = Request::_call(
            '/identity/oauth/token',
            [
                'grant_type' => self::GRANT_TYPE,
                'client_id' => $config->getClientID(),
                'client_secret' => $config->getClientSecret()
            ]
        );

        // Check if there was an error in the response
        if( property_exists( $response, 'error' ) ) {
            throw new OAuthException($response->get('error_description'));
        }

        return $response;
    }

}