<?php

namespace BR\Marketo;

/**
 * Class Email
 * @package Marketo
 * @description Handles interaction for emails via the API
 * @link http://developers.marketo.com/rest-api/assets/emails/
 */

class Email {

    /**
     * Creates an email in Marketo based on the given template
     *
     * @param int $templateID
     *      Id of the parent template
     * @param string $name
     *      Name of the email
     * @param array $folder
     *      Representation of parent folder, with members 'id', and 'type' which may be 'Folder' or 'Program'
     * @param string $fromEmail
     *      From-address of the Email
     * @param string $fromName
     *      From-name of the Email
     * @param string $replyEmail
     *      Reply-To address of the Email
     * @param string $subject
     *      Subject Line of the Email
     * @param string $description
     *      (optional) Description of the asset
     *
     * @return Response
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/emails/#create_and_update
     */
    public static function createFromTemplate( int $templateID, string $name, array $folder, string $fromEmail, string $fromName, string $replyEmail, string $subject, string $description = '' ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/emails.json',
            [
                'access_token' => $config->getAuthToken()
            ],
            [
                'template' => (int) $templateID,
                'name' => $name,
                'folder' => json_encode( $folder ),
                'fromEmail' => $fromEmail,
                'fromName' => $fromName,
                'replayEmail' => $replyEmail,
                'subject' => $subject,
                'description' => $description
            ]
        );
    }

    /**
     * Gathers the data for the email with the given ID
     *
     * @param int $id
     *      ID of the Email to get
     *
     * @return Response
     *
     * @throws CurlException
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/emails/#by_id
     */
    public static function getByID( int $id ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/email/' . $id . '.json',
            [
                'access_token' => $config->getAuthToken()
            ]
        );
    }

    public static function update( $id, $props ) {
        $config = Config::getInstance();

        $props['access_token'] = $config->getAuthToken();

        return Request::_call( '/rest/asset/v1/email/' . $id . '.json',
            [
                'access_token' => $config->getAuthToken()
            ], $props
        );
    }

    public static function updateContent( $id, $props ) {
        $config = Config::getInstance();

        $props['access_token'] = $config->getAuthToken();

        return Request::_call( '/rest/asset/v1/email/' . $id . '/content.json',
            [
                'access_token' => $config->getAuthToken()
            ], $props
        );
    }

    public static function getContentForEmail( $id ) {
        $config = Config::getInstance();

        return Request::_call(
            '/rest/asset/v1/email/'.$id.'/content.json',
            [
                'access_token' => $config->getAuthToken()
            ]
        );
    }

    /**
     * Sets an editable content area for the email with he given ID
     *
     * @param $emailID
     *      ID of the Email
     * @param $contentID
     *      ID of the content area to edit
     * @param $content
     *      The HTML to fill the content area
     *
     * @return Response
     *
     * @throws MarketoException
     *
     * @see Config::getInstance()
     * @see Request::_call()
     *
     * @link http://developers.marketo.com/rest-api/assets/emails/#content_section_type_and_update
     */
    public static function setEmailContent( $emailID, $contentID, $content ):Response {

        // Bring in the config settings
        $config = Config::getInstance();

        // perform the request
        return Request::_call(
            '/rest/asset/v1/email/'.$emailID.'/content/'.$contentID.'.json',
            [
                'access_token' => $config->getAuthToken()
            ],
            [
                'type' => 'Text',
                'value' => $content
            ]
        );
    }

}